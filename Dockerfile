FROM node:latest

RUN mkdir -p /opt/app

ADD ./package.json /tmp/package.json

RUN cd /tmp && \
    npm install && \
    rm -rf /opt/app/node_modules && \
    cp -a /tmp/node_modules /opt/app/node_modules

ADD . /opt/app/

WORKDIR /opt/app

CMD ["npm", "run", "build"]
