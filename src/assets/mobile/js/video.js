'use strict';

// Video pages
(function (window, document) {
    document.addEventListener('DOMContentLoaded', function () {
        // Get all video pages
        var pages = window.$$('.page--video');

        // Loop through each element and initialize each page
        for (var i = 0; i < pages.length; i++) {
            init(pages[i]);
        }

        function init (page) {
            // The play button icon
            var playButton = page.querySelector('.video__play-button');

            // Unique event listener for this page's button clicks
            var buttonClickListener = window.sm.bind(handlePlayButtonClick, page);

            // Unique event listener for this page's video ending
            var videoEndedListener = window.sm.bind(handleVideoEnded, page);

            // The root element to place the video in
            var root = page.querySelector('.lazy-video');

            // Safely get and parse attributes
            var attributes = {};
            try {
                if (root.dataset.attributes) {
                    attributes = JSON.parse(root.dataset.attributes);
                }
            } catch (error) {
                // catch error
            }

            // Initialize the lazy-video component
            window.sm.lazyVideo({
                root: root,
                video: {
                    src: root.dataset.lazySrc,
                    attributes: attributes
                },
                sentinels: {
                    before: root.querySelector('.sentinel--before'),
                    after: root.querySelector('.sentinel--after')
                },
                onLoad: function (video) {
                    playButton.addEventListener('click', buttonClickListener);

                    video.addEventListener('durationchange', handleVideoDurationChange);

                    video.addEventListener('ended', videoEndedListener);
                },
                onUnload: function () {
                    playButton.removeEventListener('click', buttonClickListener);
                },
                rootLeave: function () {
                    var video = page.querySelector('video');

                    // Video may not exist (possibly a bug in the observer api on page load)
                    if (video) {
                        var root = page.querySelector('.lazy-video');

                        root.classList.remove('playing');
                        video.pause();
                        video.currentTime = 0;
                    }
                }
            });
        }

        function handlePlayButtonClick (page) {
            var root = page.querySelector('.lazy-video');
            var video = page.querySelector('video');

            root.classList.add('playing');
            video.play();
            video.setAttribute('controls', true);

            window.sm.analytics.send({
                action: window.sm.analytics.METRIC_EVENT.PLAY_VIDEO
            });
        }

        function handleVideoDurationChange (event) {
            var video = event.target;

            // Play/Pause will throw an error sometimes even if it works
            try {
                video.play();
                video.pause();
            } catch (error) {
                // Catch error
            }
        }

        function handleVideoEnded (page) {
            var root = page.querySelector('.lazy-video');
            var video = page.querySelector('video');

            root.classList.remove('playing');
            video.pause();
            video.currentTime = 0;
            video.removeAttribute('controls');
        }
    });
})(window, document);
