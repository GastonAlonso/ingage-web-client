'use strict';

// Global `sm` api
(function (window, document) {
    // Declare global namespace
    /**
     * @global {Object} sm
     * @description Global ScrollMotion namespace
     */
    window.sm = window.sm || {};

    /**
     * @method sm.toArray
     * @description Convert an array-like object to an array
     *
     * @param {Any} arr The array-like object to convert
     *
     * @return {Array<Any>} result The converted array
     *
     * @example {js}
     *  const arrLike = {
     *      1: 'a',
     *      2: 'b'
     *  }
     *
     *  const result = window.sm.toArray(arrLike)
     *  // -> ['a', 'b']
     */
    window.sm.toArray = function (arr) {
        return Array.prototype.slice.call(arr, 0);
    };

    /**
     * @method sm.$
     * @description Select one DOM element using CSS selectors
     *
     * @param {String} selector The CSS selector to use when querying the DOM
     *
     * @return {HTMLElement} element The DOM node resulting from your query
     */
    window.$ = document.querySelector.bind(document);

    /**
     * @method sm.$$
     * @description Select one or more DOM elements using CSS selectors
     *
     * @param {String} selector The CSS selector to use when querying the DOM
     *
     * @return {Array<HTMLElement>} elements An array of DOM nodes resulting from your query
     */
    window.$$ = function (selector) {
        var elements = document.querySelectorAll(selector);

        return window.sm.toArray(elements);
    };

    /**
     * @method sm.clamp
     * @description Force a number to fit within a range
     *
     * @param {Number} min The low bound
     * @param {Number} max The high bound
     * @param {Number} num The number to fit within the range
     *
     * @return {Number} result The value of `num` when forced to fit in the range
     */
    window.clamp = function (min, max, num) {
        return Math.min(Math.max(num, min), max);
    };

    /**
     * @method sm.find
     * @description Find the first instance of a value in an array given a predicate function
     *
     * @param {Array<Any>} array An array of values
     * @param {Function} fn The predicate function
     *
     * @return {Any} value The value in the array (or null)
     */
    window.find = function (array, fn) {
        for (var i = 0; i < array.length; i++) {
            if (fn.apply(fn, [array[i], i, array])) return array[i];
        }

        return null;
    };

    /**
     * @method sm.bind
     * @description Bind arguments to a function
     *
     * @param {Function} fn The function to bind arguments to
     * @param {Any} ...args All other arguments to bind to function `fn`
     *
     * @return {Function} boundFn Function `fn` with the supplied arguments bound to it
     *
     * @example {js}
     *  const add = (x, y) => x + y
     *
     *  const addOne = sm.bind(add, 1)
     *
     *  addOne(1) // === 2
     *  addOne(2) // === 3
     */
    window.sm.bind = function () {
        var fn = arguments[0];
        var args = window.sm.toArray(arguments).slice(1);

        return function () {
            return fn.apply(
                fn,
                args.concat(window.sm.toArray(arguments))
            );
        };
    };

    /**
     * @method sm.lazyImage
     * @description Initialize a lazy-image component
     *
     * @param {Object} options The configuration object
     *
     * @param {Object} options.image Configuration data for the image element
     * @param {String} options.image.src The source for the image element
     * @param {Object} options.image.attributes A map of attribute keys to values
     *
     * @param {Object} options.sentinels Sentinel elements to trigger loading of the image element
     * @param {HTMLElement} options.sentinels.before A sentinel before the image
     * @param {HTMLElement} options.sentinels.after A sentinal after the image
     *
     * @param {Function} options.onLoad A callback function for when the image is injected into the DOM
     * @param {Function} options.onUnload A callback function for when the image is removed from the DOM
     *
     * @param {Function} options.rootLeave A callback function for when the root elementgoes out of view
     *
     * @return void
     */
    window.sm.lazyImage = function (options) {
        // Configuration for the intersection observer api
        var lazyImageObserverOptions = [0, 0.25, 0.5, 0.75, 1];

        // Attempt to create a new intersection observer
        try {
            var observer = new IntersectionObserver(
                handleVisibilityChange,
                lazyImageObserverOptions
            );

            observer.observe(options.root);

            if (options.sentinels) {
                if (options.sentinels.before) {
                    observer.observe(options.sentinels.before);
                }

                if (options.sentinels.after) {
                    observer.observe(options.sentinels.after);
                }
            }
        } catch (error) {
            // catch error
        }

        function loadImage () {
            // Check to see if the image exists already
            if (options.root.classList.contains('loaded')) {
                // Do nothing
                return;
            }

            // Create image element
            var image = document.createElement('img');

            // Set the image source
            image.src = options.image.src;

            // Set each attribute on the image
            var attributes = Object.keys(options.image.attributes);

            for (var i = 0; i < attributes.length; i++) {
                var attribute = attributes[i];

                image.setAttribute(attribute, options.image.attributes[attribute]);
            }

            // Mark the image as loaded
            options.root.classList.add('loaded');

            // Add the image to the root element
            options.root.appendChild(image);

            // Call onLoad listener if it exists
            if (options.onLoad) {
                options.onLoad(image);
            }
        }

        function handleVisibilityChange (entries) {
            for (var i = 0; i < entries.length; i++) {
                var entry = entries[i];

                // The image element's container
                if (entry.target === options.root) {
                    // If the container is within the viewport
                    if (entry.intersectionRatio > 0) {
                        loadImage();
                    }

                    // Otherwise, the container is not visible
                    else {
                        if (options.rootLeave) {
                            options.rootLeave();
                        }
                    }
                }

                // Sentinel elements
                if (options.sentinels) {
                    // Sentinel before the image element
                    if (entry.target === options.sentinels.before) {
                        // If the sentinel is above or in the viewport, load the image
                        if (entry.boundingClientRect.top >= entry.rootBounds.height) {
                            loadImage();
                        }
                    }

                    // Sentinel after the image element
                    if (entry.target === options.sentinels.after) {
                        // If the sentinel is in or below the viewport, load the image
                        if (entry.boundingClientRect.top >= 0) {
                            loadImage();
                        }
                    }
                }

                if (options.onIntersect) {
                    options.onIntersect(entries);
                }
            }
        }
    };

    /**
     * @method sm.lazyVideo
     * @description Initialize a lazy-video component
     *
     * @param {Object} options The configuration object
     *
     * @param {Object} options.video Configuration data for the video element
     * @param {String} options.video.src The source for the video element
     * @param {Object} options.video.attributes A map of attribute keys to values
     *
     * @param {Object} options.sentinels Sentinel elements to trigger loading of the video element
     * @param {HTMLElement} options.sentinels.before A sentinel before the video
     * @param {HTMLElement} options.sentinels.after A sentinal after the video
     *
     * @param {Function} options.onLoad A callback function for when the video is injected into the DOM
     * @param {Function} options.onUnload A callback function for when the video is removed from the DOM
     *
     * @param {Function} options.rootLeave A callback function for when the root elementgoes out of view
     *
     * @return void
     */
    window.sm.lazyVideo = function (options) {
        // Configuration for the intersection observer api
        var lazyVideoObserverOptions = [0, 0.25, 0.5, 0.75, 1];

        // Attempt to create a new intersection observer
        try {
            var observer = new IntersectionObserver(
                handleVisibilityChange,
                lazyVideoObserverOptions
            );

            observer.observe(options.root);

            if (options.sentinels) {
                if (options.sentinels.before) {
                    observer.observe(options.sentinels.before);
                }

                if (options.sentinels.after) {
                    observer.observe(options.sentinels.after);
                }
            }
        } catch (error) {
            // catch error
        }

        function unloadVideo () {
            var video = options.root.querySelector('video');

            // If the video is in the dom, remove it
            if (video) {
                video.parentNode.removeChild(video);
            }

            // Mark the video as unloaded
            options.root.classList.remove('loaded');

            // Call onUnload listener if it exists
            if (options.onUnload) {
                options.onUnload(video);
            }
        }

        function loadVideo () {
            // Check to see if the video exists already
            if (options.root.classList.contains('loaded')) {
                // Do nothing
                return;
            }

            // Create video element
            var video = document.createElement('video');

            // Set the video source
            video.src = options.video.src;

            // Set each attribute on the video
            var attributes = Object.keys(options.video.attributes);

            for (var i = 0; i < attributes.length; i++) {
                var attribute = attributes[i];

                video.setAttribute(attribute, options.video.attributes[attribute]);
            }

            // Mark the video as loaded
            options.root.classList.add('loaded');

            // Add the video to the root element
            options.root.appendChild(video);

            // Call onLoad listener if it exists
            if (options.onLoad) {
                options.onLoad(video);
            }
        }

        function handleVisibilityChange (entries) {
            var entry = entries[0];

            // The video element's container
            if (entry.target === options.root) {
                // If the container is within the viewport
                if (entry.intersectionRatio > 0) {
                    loadVideo();
                }

                // Otherwise, the container is not visible
                else {
                    if (options.rootLeave) {
                        options.rootLeave();
                    }
                }
            }

            // Sentinel elements
            if (options.sentinels) {
                // Sentinel before the video element
                if (entry.target === options.sentinels.before) {
                    // If the sentinel is below the viewport, unload the video
                    if (entry.boundingClientRect.top > entry.rootBounds.height) {
                        unloadVideo();
                    }

                    // Otherwise, load the video
                    else {
                        loadVideo();
                    }
                }

                // Sentinel after the video element
                if (entry.target === options.sentinels.after) {
                    // If the sentinel is above the viewport, unload the video
                    if (entry.boundingClientRect.top < 0) {
                        unloadVideo();
                    }

                    // Otherwise, load the video
                    else {
                        loadVideo();
                    }
                }
            }
        }
    };

    /**
     * @method sm.http
     * @description Make HTTP requests
     *
     * @param {Object} options
     * @param {String} options.method The HTTP method to use
     * @param {String} options.url The URL to request
     * @param {Object} options.body The body of the request
     * @param {Object} options.headers A map of headers to add to the request
     * @param {Function} options.onLoad A callback to get the result of the request
     * @param {Function} options.onError A callback to get the error of the request
     *
     * @return void
     */
    window.sm.http = function (options) {
        if (!options.method || !options.url) return;

        var request = new XMLHttpRequest();
        request.open(options.method.toUpperCase(), options.url, true);

        if (options.headers) {
            var headerKeys = Object.keys(options.headers);

            for (var i = 0; i < headerKeys.length; i++) {
                var key = headerKeys[i];
                var value = options.headers[key];

                request.setRequestHeader(key, value);
            }
        }

        if (options.onLoad) {
            request.addEventListener('load', function (event) {
                options.onLoad(request, event);
            });
        }

        if (options.onError) {
            request.addEventListener('error', function (event) {
                options.onError(request, event);
            });
        }

        request.send(options.body);
    };

    /**
     * JavaScript Client Detection
     * (C) viazenetti GmbH (Christian Ludwig)
     */
    window.sm.platform = (function (window) {
        var unknown = '-';

        // browser
        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browser = navigator.appName;
        var version = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // Opera
        if ((verOffset = nAgt.indexOf('Opera')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 6);

            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }

        // Opera Next
        if ((verOffset = nAgt.indexOf('OPR')) != -1) {
            browser = 'Opera';
            version = nAgt.substring(verOffset + 4);
        }

        // Edge
        else if ((verOffset = nAgt.indexOf('Edge')) != -1) {
            browser = 'Microsoft Edge';
            version = nAgt.substring(verOffset + 5);
        }

        // MSIE
        else if ((verOffset = nAgt.indexOf('MSIE')) != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(verOffset + 5);
        }

        // Chrome
        else if ((verOffset = nAgt.indexOf('Chrome')) != -1) {
            browser = 'Chrome';
            version = nAgt.substring(verOffset + 7);
        }

        // Safari
        else if ((verOffset = nAgt.indexOf('Safari')) != -1) {
            browser = 'Safari';
            version = nAgt.substring(verOffset + 7);

            if ((verOffset = nAgt.indexOf('Version')) != -1) {
                version = nAgt.substring(verOffset + 8);
            }
        }

        // Firefox
        else if ((verOffset = nAgt.indexOf('Firefox')) != -1) {
            browser = 'Firefox';
            version = nAgt.substring(verOffset + 8);
        }

        // MSIE 11+
        else if (nAgt.indexOf('Trident/') != -1) {
            browser = 'Microsoft Internet Explorer';
            version = nAgt.substring(nAgt.indexOf('rv:') + 3);
        }

        // Other browsers
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) < (verOffset = nAgt.lastIndexOf('/'))) {
            browser = nAgt.substring(nameOffset, verOffset);
            version = nAgt.substring(verOffset + 1);
            if (browser.toLowerCase() == browser.toUpperCase()) {
                browser = navigator.appName;
            }
        }

        // trim the version string
        if ((ix = version.indexOf(';')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(' ')) != -1) version = version.substring(0, ix);
        if ((ix = version.indexOf(')')) != -1) version = version.substring(0, ix);

        majorVersion = parseInt('' + version, 10);

        if (isNaN(majorVersion)) {
            version = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }

        // system
        var os = unknown;

        var clientStrings = [
            {s:'Windows 10', r:/(Windows 10.0|Windows NT 10.0)/},
            {s:'Windows 8.1', r:/(Windows 8.1|Windows NT 6.3)/},
            {s:'Windows 8', r:/(Windows 8|Windows NT 6.2)/},
            {s:'Windows 7', r:/(Windows 7|Windows NT 6.1)/},
            {s:'Windows Vista', r:/Windows NT 6.0/},
            {s:'Windows Server 2003', r:/Windows NT 5.2/},
            {s:'Windows XP', r:/(Windows NT 5.1|Windows XP)/},
            {s:'Windows 2000', r:/(Windows NT 5.0|Windows 2000)/},
            {s:'Windows ME', r:/(Win 9x 4.90|Windows ME)/},
            {s:'Windows 98', r:/(Windows 98|Win98)/},
            {s:'Windows 95', r:/(Windows 95|Win95|Windows_95)/},
            {s:'Windows NT 4.0', r:/(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/},
            {s:'Windows CE', r:/Windows CE/},
            {s:'Windows 3.11', r:/Win16/},
            {s:'Android', r:/Android/},
            {s:'Open BSD', r:/OpenBSD/},
            {s:'Sun OS', r:/SunOS/},
            {s:'Linux', r:/(Linux|X11)/},
            {s:'iOS', r:/(iPhone|iPad|iPod)/},
            {s:'Mac OS X', r:/Mac OS X/},
            {s:'Mac OS', r:/(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/},
            {s:'QNX', r:/QNX/},
            {s:'UNIX', r:/UNIX/},
            {s:'BeOS', r:/BeOS/},
            {s:'OS/2', r:/OS\/2/},
            {s:'Search Bot', r:/(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/}
        ];

        for (var id in clientStrings) {
            var cs = clientStrings[id];

            if (cs.r.test(nAgt)) {
                os = cs.s;
                break;
            }
        }

        var osVersion = unknown;

        if (/Windows/.test(os)) {
            osVersion = /Windows (.*)/.exec(os)[1];
            os = 'Windows';
        }

        switch (os) {
            case 'Mac OS X':
                osVersion = /Mac OS X (10[\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'Android':
                osVersion = /Android ([\.\_\d]+)/.exec(nAgt)[1];
                break;

            case 'iOS':
                osVersion = /OS (\d+)_(\d+)_?(\d+)?/.exec(nVer);
                osVersion = osVersion[1] + '.' + osVersion[2] + '.' + (osVersion[3] | 0);
                break;
        }

        osVersion = osVersion.replace(/_/g, '.');

        return {
            browser: browser,
            browserVersion: version,
            browserMajorVersion: majorVersion,
            os: os,
            osVersion: osVersion
        };
    })(window);
})(window, document);
