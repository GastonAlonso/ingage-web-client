'use strict';

(function (window) {
    document.addEventListener('DOMContentLoaded', function () {
        var images = window.$$('.page--text-and-image img[data-lazy-src]');

        for (var i = 0; i < images.length; i++) {
            images[i].src = images[i].dataset.lazySrc;
        }
    });
})(window);
