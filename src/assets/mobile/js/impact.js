'use strict';

(function (window) {
    document.addEventListener('DOMContentLoaded', function () {
        // Get all impact pages
        var pages = window.$$('.page--impact');

        // Loop through each impact page and initialize them
        for (var i = 0; i < pages.length; i++) {
            var page = pages[i];

            // Initialize page based on content type
            if (page.classList.contains('impact--image')) {
                initImageImpact(page);
            }

            else if (page.classList.contains('impact--video')) {
                initVideoImpact(page);
            }
        }

        function initImageImpact (page) {
            var root = page.querySelector('img');
            root.src = root.dataset.lazySrc;
        }

        function initVideoImpact (page) {
            var root = page.querySelector('.lazy-video');

            // Safely get and parse attributes
            var attributes = {};
            try {
                if (root.dataset.attributes) {
                    attributes = JSON.parse(root.dataset.attributes);
                }
            } catch (error) {
                // catch error
            }

            window.sm.lazyVideo({
                root: root,
                video: {
                    src: root.dataset.lazySrc,
                    attributes: attributes
                },
                sentinels: {
                    before: root.querySelector('.sentinel--before'),
                    after: root.querySelector('.sentinel--after')
                },
                onLoad: function (video) {
                    video.play();
                }
            });
        }

        var videos = window.$$('.page--impact video');

        for (var j = 0; j < videos.length; j++) {
            videos[j].src = videos[j].dataset.src;

        }
    });
})(window);
