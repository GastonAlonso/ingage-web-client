'use strict';

(function (window) {
    document.addEventListener('DOMContentLoaded', function () {
        var covers = window.$$('.cover');

        for (var i = 0; i < covers.length; i++) {
            var cover = covers[i];

            if (cover.classList.contains('cover--image')) {
                initImageCover(cover);
            }
        }

        function initImageCover (cover) {
            var root = cover.querySelector('.lazy-image');

            window.sm.lazyImage({
                root: root,
                image: {
                    src: root.dataset.lazySrc,
                    attributes: {}
                },
                sentinels: {
                    before: root.querySelector('.sentinel--before'),
                    after: root.querySelector('.sentinel--after')
                }
            });
        }
    });
})(window);
