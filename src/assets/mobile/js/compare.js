'use strict';

(function (window) {
    /**
     * Pixel threshold at which the compare bar
     * will bounce back to when released.
     */
    var PIXEL_THRESHOLD = 35;

    document.addEventListener('DOMContentLoaded', function () {
        var images = window.$$('.page--compare img[data-lazy-src]');
        var widgets = window.$$('.page--compare .page__media');

        for (var i = 0; i < images.length; i++) {
            images[i].src = images[i].dataset.lazySrc;
        }

        for (var i = 0; i < widgets.length; i++) {
            var widget = widgets[i];
            var isHorizontal = widget.classList.contains('page__media--horizontal');

            initCompareWidget(widget, isHorizontal);
        }
    });

    /**
     * Lazy load the compare widget's images
     */
    function lazyLoadImages (widget) {
        var images = widget.querySelectorAll('.compare__image');

        var first = images[0];
        var second = images[1];

        window.sm.lazyImage({
            root: first,
            image: {
                src: first.dataset.lazySrc,
                attributes: {}
            },
            sentinels: {
                before: widget.querySelector('.sentinel--before'),
                after: widget.querySelector('.sentinel--after')
            },
            onLoad: function () {
                var image = document.createElement('img');

                image.src = second.dataset.lazySrc;

                second.appendChild(image);
            }
        });
    }

    /**
     * Initialize compare widget interactivity
     */
    function initCompareWidget (widget, isHorizontal) {
        lazyLoadImages(widget);

        var handle = widget.querySelector('.compare__handle');
        var slider = widget.querySelector('.compare__slider');
        var image = widget.querySelector('.compare__overlay-image');

        // widget dimensions
        var widgetDims = null;

        // handle dimensions
        var handleDims = null;

        // distance from handle center
        var centerAdjust = null;

        // tracks id of initial touch event
        var touchId = null;

        var handleMouseDown = function (evt) {
            var evtCoords = { x: evt.clientX, y: evt.clientY };

            onStart(evtCoords);

            // add document listeners after interaction starts
            document.addEventListener('mousemove', handleMouseMove);
            document.addEventListener('mouseup', handleMouseUp);
        };

        var handleMouseMove = function (evt) {
            var evtCoords = { x: evt.clientX, y: evt.clientY };

            onMove(evtCoords);
        };

        var handleMouseUp = function (evt) {
            var evtCoords = { x: evt.clientX, y: evt.clientY };

            onStop(evtCoords);

            // remove document listeners when interaction stops
            document.removeEventListener('mousemove', handleMouseMove);
            document.removeEventListener('mouseup', handleMouseUp);
        };

        var handleTouchStart = function (evt) {
            // save touch identifier
            touchId = getTouchId(evt);

            // find touch by id
            var touch = findTouch(evt, touchId);

            // return if no touch was found
            if (!touch) return;

            var evtCoords = { x: touch.clientX, y: touch.clientY };

            // prevent scrolling when
            // dragging using touch
            evt.preventDefault();

            onStart(evtCoords);

            // add document listeners after interaction starts
            document.addEventListener('touchmove', handleTouchMove);
            document.addEventListener('touchend', handleTouchEnd);
        };

        var handleTouchMove = function (evt) {
            // find touch by id
            var touch = findTouch(evt, touchId);

            // return if no touch was found
            if (!touch) return;

            var evtCoords = { x: touch.clientX, y: touch.clientY };

            onMove(evtCoords);
        }

        var handleTouchEnd = function (evt) {
            // find touch by id
            var touch = findTouch(evt, touchId);

            // return if no touch was found
            if (!touch) return;

            var evtCoords = { x: touch.clientX, y: touch.clientY };

            onStop(evtCoords);

            // remove document listeners when interaction stops
            document.removeEventListener('touchmove', handleTouchMove);
            document.removeEventListener('touchend', handleTouchEnd);
        };

        var onStart = function (evtCoords) {
            // get handle and widget dimensions on start
            widgetDims = widget.getBoundingClientRect();
            handleDims = handle.getBoundingClientRect();

            // get distance from handle center to adjust event position
            centerAdjust = getCenterAdjust(evtCoords, handleDims);

            // add start class
            widget.classList.add('page__media--dragging');

            // remove stop class
            widget.classList.remove('page__media--bounce');

            window.sm.analytics.send({
                action: window.sm.analytics.METRIC_EVENT.MOVE_COMPARE
            });
        };

        var onMove = function (evtCoords) {
            // get current event position
            var position = getEventPosition(evtCoords, centerAdjust, widgetDims);

            // apply position to style
            applyStyle(position);
        };

        var onStop = function (evtCoords) {
            // remove start class
            widget.classList.remove('page__media--dragging');

            // add stop class
            widget.classList.add('page__media--bounce');

            // get final position
            var position = getEventPosition(evtCoords, centerAdjust, widgetDims);

            // clamp position within a threshold
            position = clampToThreshold(position, widgetDims);

            // apply position to style
            applyStyle(position);
        };

        // apply style for horizontal widget
        var applyHorizontalStyle = function (position) {
            slider.style.transform = 'translateX(' + position.x + '%)';
            image.style.width = position.x + '%';
        };

        // apply style for vertical widget
        var applyVerticalStyle = function (position) {
            slider.style.transform = 'translateY(' + position.y + '%)';
            image.style.height = position.y + '%';
        };

        // apply style based on widget direction
        var applyStyle = isHorizontal
            ? applyHorizontalStyle : applyVerticalStyle;

        // listen to handle events to start interaction
        handle.addEventListener('touchstart', handleTouchStart)
        handle.addEventListener('mousedown', handleMouseDown)
    }

    /**
     * Clamp final slider position to pixel threshold
     */
    function clampToThreshold (position, widgetDims) {
        // get widget dimensions
        var width = widgetDims.width;
        var height = widgetDims.height;

        // calculate minimum threshold percentage
        var xMinThreshold = PIXEL_THRESHOLD / width * 100;
        var yMinThreshold = PIXEL_THRESHOLD / height * 100;

        // calculate maximum threshold
        var xMaxThreshold = 100 - xMinThreshold;
        var yMaxThreshold = 100 - yMinThreshold;

        // clamp final position to threshold
        position.x = window.clamp(xMinThreshold, xMaxThreshold, position.x);
        position.y = window.clamp(yMinThreshold, yMaxThreshold, position.y);

        return position;
    }

    /**
     * Get distance from the handle center point
     * to the coordinates of the start event.
     */
    function getCenterAdjust (evtCoords, handleDims) {
        // get handle dimensions
        var top = handleDims.top;
        var left = handleDims.left;
        var width = handleDims.width;
        var height = handleDims.height;

        // Calculate handle center coords
        var centerX = left + width / 2;
        var centerY = top + height / 2;

        // return the difference between
        // event coords and center coords
        return {
            x: evtCoords.x - centerX,
            y: evtCoords.y - centerY
        };
    }

    /**
     * Get the event position in percentage
     */
    function getEventPosition (evtCoords, centerAdjust, widgetDims) {
        // adjust coordinates to handle center
        var adjustedX = evtCoords.x - centerAdjust.x;
        var adjustedY = evtCoords.y - centerAdjust.y;

        // get widget dimensions
        var top = widgetDims.top;
        var left = widgetDims.left;
        var width = widgetDims.width;
        var height = widgetDims.height;

        // calculate slide percentage based on
        // event position and widget dimensions
        var xPercent = ((adjustedX - left) / width) * 100;
        var yPercent = ((adjustedY - top) / height) * 100;

        // ignore mouse movement outside widget
        return {
            x: window.clamp(0, 100, xPercent),
            y: window.clamp(0, 100, yPercent)
        };
    }

    /**
     * Gets id of the original touch event
     */
    function getTouchId (evt) {
        if (evt.targetTouches && evt.targetTouches[0]) {
            return evt.targetTouches[0].identifier;
        }

        if (evt.changedTouches && evt.changedTouches[0]) {
            return evt.targetTouches[0].identifier;
        }
    }

    /**
     * Finds touch event with the given id
     */
    function findTouch (evt, touchId) {
        var touchIdMatches = function (touch) {
            return touch.identifier === touchId;
        };

        return (evt.targetTouches && window.find(evt.targetTouches, touchIdMatches))
            || (evt.changedTouches && window.find(evt.changedTouches, touchIdMatches));
    }
})(window);
