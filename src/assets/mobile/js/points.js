'use strict';

(function (window) {
    document.addEventListener('DOMContentLoaded', function () {
        var overlays = window.$$('.point-overlay');

        for (var i = 0; i < overlays.length; i++) {
            initPointsOverlay(overlays[i]);
        }
    });

    /**
     * Initialize points overlay interactivity
     */
    function initPointsOverlay (overlay) {
        // elements
        var points = overlay.querySelectorAll('.point');
        var gallery = overlay.querySelector('.point-gallery');
        var galleryContent = overlay.querySelector('.gallery__content');

        // gallery buttons
        var previous = overlay.querySelector('.gallery-button__previous');
        var next = overlay.querySelector('.gallery-button__next');
        var close = overlay.querySelector('.gallery-button__close');

        // gallery elements
        var position = overlay.querySelector('.position');
        var slides = gallery.querySelectorAll('.slide');

        var duplicateFirstSlide = slides[0].cloneNode(true);
        var duplicateLastSlide = slides[slides.length - 1].cloneNode(true);

        galleryContent.appendChild(duplicateFirstSlide);
        galleryContent.insertBefore(duplicateLastSlide, slides[0]);

        // update with new slides
        slides = gallery.querySelectorAll('.slide');

        var currentSlide = 0;
        var slideWidth = 0;
        
        // scrolling animation time (ms)
        var scrollAnimationTime = 300;

        var handleCloseClick = function () {
            // hide gallery
            gallery.classList.add('hidden');

            // reset slide
            resetSlide(currentSlide);

            // allow scrolling
            document.body.classList.remove('disable-scroll');

            // remove listeners
            close.removeEventListener('click', handleCloseClick);
            previous.removeEventListener('click', goToPreviousSlide);
            next.removeEventListener('click', goToNextSlide);
            galleryContent.removeEventListener('touchstart', onGalleryTouchStart);

            for (var i = 0; i < slides.length; i++) {
                removeEventListeners(i);
            }
        };

        var handleVideoEnded = function () {
            // get play button
            var playButton = this.parentNode.querySelector('.play-button');

            // restart video
            this.currentTime = 0;

            // hide controls
            this.controls = false;

            // show play button
            playButton.classList.remove('hidden');
        };

        var handleVideoPlayClick = function () {
            // get video element
            var video = this.parentNode.querySelector('.video');

            // hide play button
            this.classList.add('hidden');

            // add attributes
            video.controls = true;

            // play video
            video.play();

            // add listener
            video.addEventListener('ended', handleVideoEnded);

            // send analytics event
            window.sm.analytics.send({
                action: window.sm.analytics.METRIC_EVENT.PLAY_POI_VIDEO
            });
        };

        var handleAudioEnded = function () {
            // get audio button
            var audioButton = this.parentNode.querySelector('.audio-button');

            // remove playing class
            audioButton.classList.remove('audio-button--playing');
        };

        var handleAudioClick = function () {
            // get audio element
            var audio = this.parentNode.querySelector('.audio');

            // pause audio if playing
            if (audio.currentTime !== 0 && !audio.paused) {
                // remove playing class
                this.classList.remove('audio-button--playing');

                audio.pause();
            }

            // play audio
            else {
                // add playing class
                this.classList.add('audio-button--playing');

                audio.play();

                // send analytics event
                window.sm.analytics.send({
                    action: window.sm.analytics.METRIC_EVENT.PLAY_POI_AUDIO
                });
            }
        };

        var generateMapUrl = function (data, embed) {
            var name = data.name || '';
            var address = data.address || '';

            return embed ? `https://www.google.com/maps/embed/v1/place?q=${encodeURIComponent(name + ' ' + address)}&key=AIzaSyCWSXr0q_A-fQSGuFUEGcT-LEPjZZB_0pU` : `https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(name + ' ' + address)}`;
        };

        // touch start x
        var startX = 0;

        // touch start y
        var startY = 0;

        // latest drag x
        var dragX = 0;

        // how much to translate gallery container
        var galleryOffset = 0;

        var draggingHorizontal = false;
        var draggingRight = false;
        var dragStarted = false;

        // returns true if slope < 0.5
        var isDraggingHorizontal = function (firstX, firstY, secondX, secondY) {
            return Math.abs((secondY - firstY) / (secondX - firstX)) < 0.5;
        };

        var onGalleryDrag = function (evt) {
            if (dragStarted) {
                draggingHorizontal = isDraggingHorizontal(startX, startY, evt.targetTouches[0].clientX, evt.targetTouches[0].clientY);
                dragStarted = false;
            }
            
            // only drag container if dragging horizontal
            if (draggingHorizontal) {
                galleryOffset += dragX - evt.targetTouches[0].clientX;
                galleryContent.style.transform = `translateX(-${galleryOffset}px)`;
                draggingRight = evt.targetTouches[0].clientX <= dragX;
                dragX = evt.targetTouches[0].clientX;
            }
        };

        var onGalleryUp = function () {
            // how many pixels to drag to trigger next or previous slide
            var minDragLength = 10;

            // remove listeners
            galleryContent.removeEventListener('touchmove', onGalleryDrag);
            galleryContent.removeEventListener('touchend', onGalleryUp);

            // previous slide
            if (dragX > startX + minDragLength && !draggingRight) {
                goToPreviousSlide();
            }

            // next slide
            else if (dragX < startX - minDragLength && draggingRight) {
                goToNextSlide();
            }

            // center current slide
            else {
                goToSlide(currentSlide, true);
            }

            dragX = 0;
        };

        var onGalleryTouchStart = function (evt) {
            startX = evt.targetTouches[0].clientX;
            startY = evt.targetTouches[0].clientY;
            dragX = evt.targetTouches[0].clientX;

            dragStarted = true;
            galleryOffset = slideWidth * currentSlide;

            // add listeners
            galleryContent.addEventListener('touchmove', onGalleryDrag);
            galleryContent.addEventListener('touchend', onGalleryUp);
        };

        var goToSlide = function (index, scroll) {
            if (scroll) {
                galleryContent.classList.add('gallery__content--scrolling');
                setTimeout(function () {
                    galleryContent.classList.remove('gallery__content--scrolling');
                }, scrollAnimationTime);
            }

            galleryContent.style.transform = `translateX(-${index * slideWidth}px)`;
        };

        var renderSlide = function (index, fromPoint) {
            var slide = slides[index];

            switch (slide.dataset.type) {
                case 'image':
                    var image = slide.querySelector('.image');

                    if (!image.src) {
                        // set image source
                        image.src = image.dataset.image;
                    }

                    break;

                case 'video':
                    var video = slide.querySelector('.video');
                    var playButton = slide.querySelector('.play-button');

                    if (!video.src) {
                        // set video source
                        video.src = video.dataset.video;
                    } 

                    // add listener
                    playButton.addEventListener('click', handleVideoPlayClick);
                    break;

                case 'audio':
                    var audio = slide.querySelector('.audio');
                    var audioButton = slide.querySelector('.audio-button');

                    // set audio source
                    if (!audio.src) {
                        audio.src = audio.dataset.audio;
                    }

                    // play audio if audio point was clicked
                    if (fromPoint) {
                        audio.play();

                        // add playing class
                        audioButton.classList.add('audio-button--playing');
                    }

                    // add listeners
                    audioButton.addEventListener('click', handleAudioClick);
                    audio.addEventListener('ended', handleAudioEnded);

                    break;

                case 'map':
                    var map = slide.querySelector('.map');
                    var link = slide.querySelector('.body');
                    var data = JSON.parse(map.dataset.data);

                    if (!map.src) {
                        map.src = generateMapUrl(data, true);

                        link.href = generateMapUrl(data);
                    }
                    
                    break;

                default:
                    break;
            }

        };

        var updatePositionText = function (index) {
            position.innerHTML = `${index} of ${slides.length - 2}`;
        };

        var removeEventListeners = function (index) {
            var slide = slides[index];

            // reset video 
            if (slide.dataset.type === 'video') {
                var video = slide.querySelector('.video');
                var playButton = slide.querySelector('.play-button');

                // remove listeners
                playButton.removeEventListener('click', handleVideoPlayClick);
                video.removeEventListener('ended', handleVideoEnded);
            }

            // reset audio
            if (slide.dataset.type === 'audio') {
                var audio = slide.querySelector('.audio');
                var audioButton = slide.querySelector('.audio-button');

                // remove listeners
                audioButton.removeEventListener('click', handleAudioClick);
                audio.removeEventListener('ended', handleAudioEnded);
            }

        };

        var resetSlide = function (index) {
            var slide = slides[index];

            slide.scrollTop = 0;

            // reset video 
            if (slide.dataset.type === 'video') {
                var video = slide.querySelector('.video');
                var playButton = slide.querySelector('.play-button');

                // pause video
                video.pause();

                // restart video
                video.currentTime = 0;

                // hide controls
                video.controls = false;

                // show play button
                playButton.classList.remove('hidden');
            }

            // reset audio
            if (slide.dataset.type === 'audio') {
                var audio = slide.querySelector('.audio');
                var audioButton = slide.querySelector('.audio-button');

                // pause audio
                audio.pause();

                // restart audio
                audio.currentTime = 0;

                // remove class
                audioButton.classList.remove('audio-button--playing');
            }
        };

        var goToPreviousSlide = function () {
            // reset current slide
            resetSlide(currentSlide);

            currentSlide--;

            // scroll to slide
            goToSlide(currentSlide, true);

            // for infinite gallery
            if (currentSlide === 0) {
                setTimeout(function () {
                    currentSlide = slides.length - 2;
                    goToSlide(currentSlide);
                }, scrollAnimationTime);

                updatePositionText(slides.length - 2);
            }

            // update position
            else {
                updatePositionText(currentSlide);
            }
        };

        var goToNextSlide = function () {
            // reset current slide
            resetSlide(currentSlide);

            // increment slide index
            currentSlide++;

            // scroll to slide
            goToSlide(currentSlide, true);

            // for infinite gallery
            if (currentSlide === slides.length - 1) {
                setTimeout(function () {
                    currentSlide = 1;
                    goToSlide(currentSlide);
                }, scrollAnimationTime);

                updatePositionText(1);
            }

            // update position
            else {
                updatePositionText(currentSlide);
            }
        };

        var handlePointClick = function () {
            var type = this.dataset.type;

            // send analytics event
            sendOpenEvent(type);

            // website point does not open in gallery
            if (type === 'website') {
                return;
            }

            // get index
            currentSlide = parseInt(this.dataset.slideIndex) + 1;

            // show gallery
            gallery.classList.remove('hidden');

            updateSlideWidth();

            // show correct slide, play audio if audio point
            renderSlide(currentSlide, true);

            // go to slide position without scrolling
            goToSlide(currentSlide, false);

            // update text
            updatePositionText(currentSlide);

            // disable scrolling
            document.body.classList.add('disable-scroll');

            // add listeners
            close.addEventListener('click', handleCloseClick);
            previous.addEventListener('click', goToPreviousSlide);
            next.addEventListener('click', goToNextSlide);
            galleryContent.addEventListener('touchstart', onGalleryTouchStart);

            // render all other slides
            for (var i = 0; i < slides.length; i++) {
                if (i === currentSlide) continue;

                renderSlide(i);
            }
        };

        /* On window resize recalculate the slide widths and translate back
         * to the current slide
         */
        var updateSlideWidth = function () {
            slideWidth = gallery.getBoundingClientRect().width;
            goToSlide(currentSlide);
        };

        var sendOpenEvent = function (type) {
            var action = null;

            // determina action for point type
            switch (type) {
                case 'blurb':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_TEXT;
                    break;
                case 'image':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_IMAGE;
                    break;
                case 'map':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_MAP;
                    break;
                case 'email':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_EMAIL;
                    break;
                case 'video':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_VIDEO;
                    break;
                case 'audio':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_AUDIO;
                    break;
                case 'website':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_WEBSITE;
                    break;
            }

            // send event
            if (action) {
                window.sm.analytics.send({ action: action });
            }
        };

        window.addEventListener('resize', function () {
            window.requestAnimationFrame(updateSlideWidth);
        });

        // add event listeners to points
        for (var i = 0; i < points.length; i++) {
            points[i].addEventListener('click', handlePointClick);
        }
    }
})(window);
