'use strict';

(function (window) {
    document.addEventListener('DOMContentLoaded', function () {
        var footerShare = window.$('.footer__share');
        var facebookShare = window.$('.share--facebook');
        var twitterShare = window.$('.share--twitter');
        var mailShare = window.$('.share--mail');
        var learnMore = window.$('.footer__learn-more');

        var title = footerShare.dataset.title;
        var subtitle = footerShare.dataset.subtitle;

        facebookShare.addEventListener('click', function () {
            window.sm.analytics.send({
                action: window.sm.analytics.METRIC_EVENT.SHARE_FACEBOOK
            });

            window.open('https://www.facebook.com/sharer/sharer.php', 'Post to Facebook', 'width=500,height=600');
        });

        twitterShare.addEventListener('click', function () {
            window.sm.analytics.send({
                action: window.sm.analytics.METRIC_EVENT.SHARE_TWITTER
            });

            window.open('https://www.twitter.com/share', 'Share a link on Twitter', 'width=500,height=600');
        });

        mailShare.addEventListener('click', function () {
            window.sm.analytics.send({
                action: window.sm.analytics.METRIC_EVENT.SHARE_EMAIL
            });
        });

        mailShare.href = `mailto:%20?subject=${title}:%20${subtitle}&body=${title}:%20${subtitle}%0A%0A%0A${window.location.href}`;

        learnMore.addEventListener('click', function () {
            window.sm.analytics.send({
                action: window.sm.analytics.METRIC_EVENT.CLICK_MADE_WITH
            });
        });
    });
})(window);
