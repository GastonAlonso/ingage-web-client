/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the W3C SOFTWARE AND DOCUMENT NOTICE AND LICENSE.
 *
 *  https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
 * 
 */

(function(window, document) {
    'use strict';


    // Exits early if all IntersectionObserver and IntersectionObserverEntry
    // features are natively supported.
    if ('IntersectionObserver' in window &&
    'IntersectionObserverEntry' in window &&
    'intersectionRatio' in window.IntersectionObserverEntry.prototype) {

        // Minimal polyfill for Edge 15's lack of `isIntersecting`
        // See: https://github.com/w3c/IntersectionObserver/issues/211
        if (!('isIntersecting' in window.IntersectionObserverEntry.prototype)) {
            Object.defineProperty(window.IntersectionObserverEntry.prototype,
                'isIntersecting', {
                    get: function () {
                        return this.intersectionRatio > 0;
                    }
                });
        }
        return;
    }


    /**
 * An IntersectionObserver registry. This registry exists to hold a strong
 * reference to IntersectionObserver instances currently observering a target
 * element. Without this registry, instances without another reference may be
 * garbage collected.
 */
    var registry = [];


    /**
 * Creates the global IntersectionObserverEntry constructor.
 * https://w3c.github.io/IntersectionObserver/#intersection-observer-entry
 * @param {Object} entry A dictionary of instance properties.
 * @constructor
 */
    function IntersectionObserverEntry(entry) {
        this.time = entry.time;
        this.target = entry.target;
        this.rootBounds = entry.rootBounds;
        this.boundingClientRect = entry.boundingClientRect;
        this.intersectionRect = entry.intersectionRect || getEmptyRect();
        this.isIntersecting = !!entry.intersectionRect;

        // Calculates the intersection ratio.
        var targetRect = this.boundingClientRect;
        var targetArea = targetRect.width * targetRect.height;
        var intersectionRect = this.intersectionRect;
        var intersectionArea = intersectionRect.width * intersectionRect.height;

        // Sets intersection ratio.
        if (targetArea) {
            this.intersectionRatio = intersectionArea / targetArea;
        } else {
            // If area is zero and is intersecting, sets to 1, otherwise to 0
            this.intersectionRatio = this.isIntersecting ? 1 : 0;
        }
    }


    /**
 * Creates the global IntersectionObserver constructor.
 * https://w3c.github.io/IntersectionObserver/#intersection-observer-interface
 * @param {Function} callback The function to be invoked after intersection
 *     changes have queued. The function is not invoked if the queue has
 *     been emptied by calling the `takeRecords` method.
 * @param {Object=} opt_options Optional configuration options.
 * @constructor
 */
    function IntersectionObserver(callback, opt_options) {

        var options = opt_options || {};

        if (typeof callback != 'function') {
            throw new Error('callback must be a function');
        }

        if (options.root && options.root.nodeType != 1) {
            throw new Error('root must be an Element');
        }

        // Binds and throttles `this._checkForIntersections`.
        this._checkForIntersections = throttle(
            this._checkForIntersections.bind(this), this.THROTTLE_TIMEOUT);

        // Private properties.
        this._callback = callback;
        this._observationTargets = [];
        this._queuedEntries = [];
        this._rootMarginValues = this._parseRootMargin(options.rootMargin);

        // Public properties.
        this.thresholds = this._initThresholds(options.threshold);
        this.root = options.root || null;
        this.rootMargin = this._rootMarginValues.map(function(margin) {
            return margin.value + margin.unit;
        }).join(' ');
    }


    /**
 * The minimum interval within which the document will be checked for
 * intersection changes.
 */
    IntersectionObserver.prototype.THROTTLE_TIMEOUT = 100;


    /**
 * The frequency in which the polyfill polls for intersection changes.
 * this can be updated on a per instance basis and must be set prior to
 * calling `observe` on the first target.
 */
    IntersectionObserver.prototype.POLL_INTERVAL = null;


    /**
 * Starts observing a target element for intersection changes based on
 * the thresholds values.
 * @param {Element} target The DOM element to observe.
 */
    IntersectionObserver.prototype.observe = function(target) {
        var isTargetAlreadyObserved = this._observationTargets.some(function(item) {
            return item.element == target;
        });

        if (isTargetAlreadyObserved) {
            return;
        }

        if (!(target && target.nodeType == 1)) {
            throw new Error('target must be an Element');
        }

        this._registerInstance();
        this._observationTargets.push({element: target, entry: null});
        this._monitorIntersections();
        this._checkForIntersections();
    };


    /**
 * Stops observing a target element for intersection changes.
 * @param {Element} target The DOM element to observe.
 */
    IntersectionObserver.prototype.unobserve = function(target) {
        this._observationTargets =
      this._observationTargets.filter(function(item) {

          return item.element != target;
      });
        if (!this._observationTargets.length) {
            this._unmonitorIntersections();
            this._unregisterInstance();
        }
    };


    /**
 * Stops observing all target elements for intersection changes.
 */
    IntersectionObserver.prototype.disconnect = function() {
        this._observationTargets = [];
        this._unmonitorIntersections();
        this._unregisterInstance();
    };


    /**
 * Returns any queue entries that have not yet been reported to the
 * callback and clears the queue. This can be used in conjunction with the
 * callback to obtain the absolute most up-to-date intersection information.
 * @return {Array} The currently queued entries.
 */
    IntersectionObserver.prototype.takeRecords = function() {
        var records = this._queuedEntries.slice();
        this._queuedEntries = [];
        return records;
    };


    /**
 * Accepts the threshold value from the user configuration object and
 * returns a sorted array of unique threshold values. If a value is not
 * between 0 and 1 and error is thrown.
 * @private
 * @param {Array|number=} opt_threshold An optional threshold value or
 *     a list of threshold values, defaulting to [0].
 * @return {Array} A sorted list of unique and valid threshold values.
 */
    IntersectionObserver.prototype._initThresholds = function(opt_threshold) {
        var threshold = opt_threshold || [0];
        if (!Array.isArray(threshold)) threshold = [threshold];

        return threshold.sort().filter(function(t, i, a) {
            if (typeof t != 'number' || isNaN(t) || t < 0 || t > 1) {
                throw new Error('threshold must be a number between 0 and 1 inclusively');
            }
            return t !== a[i - 1];
        });
    };


    /**
 * Accepts the rootMargin value from the user configuration object
 * and returns an array of the four margin values as an object containing
 * the value and unit properties. If any of the values are not properly
 * formatted or use a unit other than px or %, and error is thrown.
 * @private
 * @param {string=} opt_rootMargin An optional rootMargin value,
 *     defaulting to '0px'.
 * @return {Array<Object>} An array of margin objects with the keys
 *     value and unit.
 */
    IntersectionObserver.prototype._parseRootMargin = function(opt_rootMargin) {
        var marginString = opt_rootMargin || '0px';
        var margins = marginString.split(/\s+/).map(function(margin) {
            var parts = /^(-?\d*\.?\d+)(px|%)$/.exec(margin);
            if (!parts) {
                throw new Error('rootMargin must be specified in pixels or percent');
            }
            return {value: parseFloat(parts[1]), unit: parts[2]};
        });

        // Handles shorthand.
        margins[1] = margins[1] || margins[0];
        margins[2] = margins[2] || margins[0];
        margins[3] = margins[3] || margins[1];

        return margins;
    };


    /**
 * Starts polling for intersection changes if the polling is not already
 * happening, and if the page's visibilty state is visible.
 * @private
 */
    IntersectionObserver.prototype._monitorIntersections = function() {
        if (!this._monitoringIntersections) {
            this._monitoringIntersections = true;

            // If a poll interval is set, use polling instead of listening to
            // resize and scroll events or DOM mutations.
            if (this.POLL_INTERVAL) {
                this._monitoringInterval = setInterval(
                    this._checkForIntersections, this.POLL_INTERVAL);
            }
            else {
                addEvent(window, 'resize', this._checkForIntersections, true);
                addEvent(document, 'scroll', this._checkForIntersections, true);

                if ('MutationObserver' in window) {
                    this._domObserver = new MutationObserver(this._checkForIntersections);
                    this._domObserver.observe(document, {
                        attributes: true,
                        childList: true,
                        characterData: true,
                        subtree: true
                    });
                }
            }
        }
    };


    /**
 * Stops polling for intersection changes.
 * @private
 */
    IntersectionObserver.prototype._unmonitorIntersections = function() {
        if (this._monitoringIntersections) {
            this._monitoringIntersections = false;

            clearInterval(this._monitoringInterval);
            this._monitoringInterval = null;

            removeEvent(window, 'resize', this._checkForIntersections, true);
            removeEvent(document, 'scroll', this._checkForIntersections, true);

            if (this._domObserver) {
                this._domObserver.disconnect();
                this._domObserver = null;
            }
        }
    };


    /**
 * Scans each observation target for intersection changes and adds them
 * to the internal entries queue. If new entries are found, it
 * schedules the callback to be invoked.
 * @private
 */
    IntersectionObserver.prototype._checkForIntersections = function() {
        var rootIsInDom = this._rootIsInDom();
        var rootRect = rootIsInDom ? this._getRootRect() : getEmptyRect();

        this._observationTargets.forEach(function(item) {
            var target = item.element;
            var targetRect = getBoundingClientRect(target);
            var rootContainsTarget = this._rootContainsTarget(target);
            var oldEntry = item.entry;
            var intersectionRect = rootIsInDom && rootContainsTarget &&
        this._computeTargetAndRootIntersection(target, rootRect);

            var newEntry = item.entry = new IntersectionObserverEntry({
                time: now(),
                target: target,
                boundingClientRect: targetRect,
                rootBounds: rootRect,
                intersectionRect: intersectionRect
            });

            if (!oldEntry) {
                this._queuedEntries.push(newEntry);
            } else if (rootIsInDom && rootContainsTarget) {
                // If the new entry intersection ratio has crossed any of the
                // thresholds, add a new entry.
                if (this._hasCrossedThreshold(oldEntry, newEntry)) {
                    this._queuedEntries.push(newEntry);
                }
            } else {
                // If the root is not in the DOM or target is not contained within
                // root but the previous entry for this target had an intersection,
                // add a new record indicating removal.
                if (oldEntry && oldEntry.isIntersecting) {
                    this._queuedEntries.push(newEntry);
                }
            }
        }, this);

        if (this._queuedEntries.length) {
            this._callback(this.takeRecords(), this);
        }
    };


    /**
 * Accepts a target and root rect computes the intersection between then
 * following the algorithm in the spec.
 * TODO(philipwalton): at this time clip-path is not considered.
 * https://w3c.github.io/IntersectionObserver/#calculate-intersection-rect-algo
 * @param {Element} target The target DOM element
 * @param {Object} rootRect The bounding rect of the root after being
 *     expanded by the rootMargin value.
 * @return {?Object} The final intersection rect object or undefined if no
 *     intersection is found.
 * @private
 */
    IntersectionObserver.prototype._computeTargetAndRootIntersection =
    function(target, rootRect) {

        // If the element isn't displayed, an intersection can't happen.
        if (window.getComputedStyle(target).display == 'none') return;

        var targetRect = getBoundingClientRect(target);
        var intersectionRect = targetRect;
        var parent = getParentNode(target);
        var atRoot = false;

        while (!atRoot) {
            var parentRect = null;
            var parentComputedStyle = parent.nodeType == 1 ?
                window.getComputedStyle(parent) : {};

            // If the parent isn't displayed, an intersection can't happen.
            if (parentComputedStyle.display == 'none') return;

            if (parent == this.root || parent == document) {
                atRoot = true;
                parentRect = rootRect;
            } else {
                // If the element has a non-visible overflow, and it's not the <body>
                // or <html> element, update the intersection rect.
                // Note: <body> and <html> cannot be clipped to a rect that's not also
                // the document rect, so no need to compute a new intersection.
                if (parent != document.body &&
          parent != document.documentElement &&
          parentComputedStyle.overflow != 'visible') {
                    parentRect = getBoundingClientRect(parent);
                }
            }

            // If either of the above conditionals set a new parentRect,
            // calculate new intersection data.
            if (parentRect) {
                intersectionRect = computeRectIntersection(parentRect, intersectionRect);

                if (!intersectionRect) break;
            }
            parent = getParentNode(parent);
        }
        return intersectionRect;
    };


    /**
 * Returns the root rect after being expanded by the rootMargin value.
 * @return {Object} The expanded root rect.
 * @private
 */
    IntersectionObserver.prototype._getRootRect = function() {
        var rootRect;
        if (this.root) {
            rootRect = getBoundingClientRect(this.root);
        } else {
            // Use <html>/<body> instead of window since scroll bars affect size.
            var html = document.documentElement;
            var body = document.body;
            rootRect = {
                top: 0,
                left: 0,
                right: html.clientWidth || body.clientWidth,
                width: html.clientWidth || body.clientWidth,
                bottom: html.clientHeight || body.clientHeight,
                height: html.clientHeight || body.clientHeight
            };
        }
        return this._expandRectByRootMargin(rootRect);
    };


    /**
 * Accepts a rect and expands it by the rootMargin value.
 * @param {Object} rect The rect object to expand.
 * @return {Object} The expanded rect.
 * @private
 */
    IntersectionObserver.prototype._expandRectByRootMargin = function(rect) {
        var margins = this._rootMarginValues.map(function(margin, i) {
            return margin.unit == 'px' ? margin.value :
                margin.value * (i % 2 ? rect.width : rect.height) / 100;
        });
        var newRect = {
            top: rect.top - margins[0],
            right: rect.right + margins[1],
            bottom: rect.bottom + margins[2],
            left: rect.left - margins[3]
        };
        newRect.width = newRect.right - newRect.left;
        newRect.height = newRect.bottom - newRect.top;

        return newRect;
    };


    /**
 * Accepts an old and new entry and returns true if at least one of the
 * threshold values has been crossed.
 * @param {?IntersectionObserverEntry} oldEntry The previous entry for a
 *    particular target element or null if no previous entry exists.
 * @param {IntersectionObserverEntry} newEntry The current entry for a
 *    particular target element.
 * @return {boolean} Returns true if a any threshold has been crossed.
 * @private
 */
    IntersectionObserver.prototype._hasCrossedThreshold =
    function(oldEntry, newEntry) {

        // To make comparing easier, an entry that has a ratio of 0
        // but does not actually intersect is given a value of -1
        var oldRatio = oldEntry && oldEntry.isIntersecting ?
            oldEntry.intersectionRatio || 0 : -1;
        var newRatio = newEntry.isIntersecting ?
            newEntry.intersectionRatio || 0 : -1;

        // Ignore unchanged ratios
        if (oldRatio === newRatio) return;

        for (var i = 0; i < this.thresholds.length; i++) {
            var threshold = this.thresholds[i];

            // Return true if an entry matches a threshold or if the new ratio
            // and the old ratio are on the opposite sides of a threshold.
            if (threshold == oldRatio || threshold == newRatio ||
        threshold < oldRatio !== threshold < newRatio) {
                return true;
            }
        }
    };


    /**
 * Returns whether or not the root element is an element and is in the DOM.
 * @return {boolean} True if the root element is an element and is in the DOM.
 * @private
 */
    IntersectionObserver.prototype._rootIsInDom = function() {
        return !this.root || containsDeep(document, this.root);
    };


    /**
 * Returns whether or not the target element is a child of root.
 * @param {Element} target The target element to check.
 * @return {boolean} True if the target element is a child of root.
 * @private
 */
    IntersectionObserver.prototype._rootContainsTarget = function(target) {
        return containsDeep(this.root || document, target);
    };


    /**
 * Adds the instance to the global IntersectionObserver registry if it isn't
 * already present.
 * @private
 */
    IntersectionObserver.prototype._registerInstance = function() {
        if (registry.indexOf(this) < 0) {
            registry.push(this);
        }
    };


    /**
 * Removes the instance from the global IntersectionObserver registry.
 * @private
 */
    IntersectionObserver.prototype._unregisterInstance = function() {
        var index = registry.indexOf(this);
        if (index != -1) registry.splice(index, 1);
    };


    /**
 * Returns the result of the performance.now() method or null in browsers
 * that don't support the API.
 * @return {number} The elapsed time since the page was requested.
 */
    function now() {
        return window.performance && performance.now && performance.now();
    }


    /**
 * Throttles a function and delays its executiong, so it's only called at most
 * once within a given time period.
 * @param {Function} fn The function to throttle.
 * @param {number} timeout The amount of time that must pass before the
 *     function can be called again.
 * @return {Function} The throttled function.
 */
    function throttle(fn, timeout) {
        var timer = null;
        return function () {
            if (!timer) {
                timer = setTimeout(function() {
                    fn();
                    timer = null;
                }, timeout);
            }
        };
    }


    /**
 * Adds an event handler to a DOM node ensuring cross-browser compatibility.
 * @param {Node} node The DOM node to add the event handler to.
 * @param {string} event The event name.
 * @param {Function} fn The event handler to add.
 * @param {boolean} opt_useCapture Optionally adds the even to the capture
 *     phase. Note: this only works in modern browsers.
 */
    function addEvent(node, event, fn, opt_useCapture) {
        if (typeof node.addEventListener == 'function') {
            node.addEventListener(event, fn, opt_useCapture || false);
        }
        else if (typeof node.attachEvent == 'function') {
            node.attachEvent('on' + event, fn);
        }
    }


    /**
 * Removes a previously added event handler from a DOM node.
 * @param {Node} node The DOM node to remove the event handler from.
 * @param {string} event The event name.
 * @param {Function} fn The event handler to remove.
 * @param {boolean} opt_useCapture If the event handler was added with this
 *     flag set to true, it should be set to true here in order to remove it.
 */
    function removeEvent(node, event, fn, opt_useCapture) {
        if (typeof node.removeEventListener == 'function') {
            node.removeEventListener(event, fn, opt_useCapture || false);
        }
        else if (typeof node.detatchEvent == 'function') {
            node.detatchEvent('on' + event, fn);
        }
    }


    /**
 * Returns the intersection between two rect objects.
 * @param {Object} rect1 The first rect.
 * @param {Object} rect2 The second rect.
 * @return {?Object} The intersection rect or undefined if no intersection
 *     is found.
 */
    function computeRectIntersection(rect1, rect2) {
        var top = Math.max(rect1.top, rect2.top);
        var bottom = Math.min(rect1.bottom, rect2.bottom);
        var left = Math.max(rect1.left, rect2.left);
        var right = Math.min(rect1.right, rect2.right);
        var width = right - left;
        var height = bottom - top;

        return (width >= 0 && height >= 0) && {
            top: top,
            bottom: bottom,
            left: left,
            right: right,
            width: width,
            height: height
        };
    }


    /**
 * Shims the native getBoundingClientRect for compatibility with older IE.
 * @param {Element} el The element whose bounding rect to get.
 * @return {Object} The (possibly shimmed) rect of the element.
 */
    function getBoundingClientRect(el) {
        var rect;

        try {
            rect = el.getBoundingClientRect();
        } catch (err) {
            // Ignore Windows 7 IE11 "Unspecified error"
            // https://github.com/w3c/IntersectionObserver/pull/205
        }

        if (!rect) return getEmptyRect();

        // Older IE
        if (!(rect.width && rect.height)) {
            rect = {
                top: rect.top,
                right: rect.right,
                bottom: rect.bottom,
                left: rect.left,
                width: rect.right - rect.left,
                height: rect.bottom - rect.top
            };
        }
        return rect;
    }


    /**
 * Returns an empty rect object. An empty rect is returned when an element
 * is not in the DOM.
 * @return {Object} The empty rect.
 */
    function getEmptyRect() {
        return {
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            width: 0,
            height: 0
        };
    }

    /**
 * Checks to see if a parent element contains a child elemnt (including inside
 * shadow DOM).
 * @param {Node} parent The parent element.
 * @param {Node} child The child element.
 * @return {boolean} True if the parent node contains the child node.
 */
    function containsDeep(parent, child) {
        var node = child;
        while (node) {
            if (node == parent) return true;

            node = getParentNode(node);
        }
        return false;
    }


    /**
 * Gets the parent node of an element or its host element if the parent node
 * is a shadow root.
 * @param {Node} node The node whose parent to get.
 * @return {Node|null} The parent node or null if no parent exists.
 */
    function getParentNode(node) {
        var parent = node.parentNode;

        if (parent && parent.nodeType == 11 && parent.host) {
            // If the parent is a shadow root, return the host element.
            return parent.host;
        }
        return parent;
    }


    // Exposes the constructors globally.
    window.IntersectionObserver = IntersectionObserver;
    window.IntersectionObserverEntry = IntersectionObserverEntry;

}(window, document));


(function (window, document) {
    /**
 * Object Fit Videos
 * Polyfill for object-fit and object-position CSS properties on video elements
 * Covers IE9, IE10, IE11, Edge, Safari <10
 *
 * Usage
 * In your CSS, add a special font-family tag for IE/Edge
 * video {
 *   object-fit: cover;
 *   font-family: 'object-fit: cover;';
 * }
 *
 * Before the closing body tag, or whenever the DOM is ready,
 * make the JavaScript call
 * objectFitVideos();
 *
 * All video elements with the special CSS property will be targeted
 *
 * @license  MIT (https://opensource.org/licenses/MIT)
 * @author   Todd Miller <todd.miller@tricomb2b.com>
 * @version  1.0.2
 * @changelog
 * 2016-08-19 - Adds object-position support.
 * 2016-08-19 - Add throttle function for more performant resize events
 * 2016-08-19 - Initial release with object-fit support, and
 *              object-position default 'center'
 * 2016-10-14 - No longer relies on window load event, instead requires a specific
 *              function call to initialize the videos for object fit and position.
 * 2016-11-28 - Support CommonJS environment, courtesy of @msorensson
 * 2016-12-05 - Refactors the throttling function to support IE
 * 2017-09-26 - Fix an issue with autplay not working on polyfilled videos
 *            - Adds the capability to specify elements to polyfill,
 *              instead of just checking every video element for the
 *              CSS property. Slight performance gain in most usecases,
 *              and a bigger gain in a few usecases.
 */
    var objectFitVideos = function (videos) {
        'use strict';

        var testImg                = new Image(),
            supportsObjectFit      = 'object-fit' in testImg.style,
            supportsObjectPosition = 'object-position' in testImg.style,
            propRegex              = /(object-fit|object-position)\s*:\s*([-\w\s%]+)/g;

        if (!supportsObjectFit || !supportsObjectPosition) {
            initialize(videos);
            throttle('resize', 'optimizedResize');
        }

        /**
   * Parse the style and look for the special font-family tag
   * @param  {object} $el The element to parse
   * @return {object}     The font-family properties we're interested in
   */
        function getStyle ($el) {
            var style  = getComputedStyle($el).fontFamily,
                parsed = null,
                props  = {};

            while ((parsed = propRegex.exec(style)) !== null) {
                props[parsed[1]] = parsed[2];
            }

            if (props['object-position'])
                return parsePosition(props);

            return props;
        }

        /**
   * Initialize all the relevant video elements and get them fitted
   */
        function initialize (videos) {
            var index = -1;

            if (!videos) {
                // if no videos given, query all video elements
                videos = document.querySelectorAll('video');
            } else if (!('length' in videos)) {
                // convert to an array for proper looping if an array or NodeList
                // was not given
                videos = [videos];
            }

            while (videos[++index]) {
                var style = getStyle(videos[index]);

                // only do work if the property is on the element
                if (style['object-fit'] || style['object-position']) {
                    // set the default values
                    style['object-fit'] = style['object-fit'] || 'fill';
                    fitIt(videos[index], style);
                }
            }
        }

        /**
   * Object Fit
   * @param  {object} $el Element to fit
   * @return {object}     The element's relevant properties
   */
        function fitIt ($el, style) {
            // fill is the default behavior, no action is necessary
            if (style['object-fit'] === 'fill')
                return;

            // convenience style properties on the source element
            var setCss = $el.style,
                getCss = window.getComputedStyle($el);

            // create and insert a wrapper element
            var $wrap = document.createElement('object-fit');
            $wrap.appendChild($el.parentNode.replaceChild($wrap, $el));

            // style the wrapper element to mostly match the source element
            var wrapCss = {
                height:    '100%',
                width:     '100%',
                boxSizing: 'content-box',
                display:   'inline-block',
                overflow:  'hidden'
            };

            'backgroundColor backgroundImage borderColor borderStyle borderWidth bottom fontSize lineHeight left opacity margin position right top visibility'.replace(/\w+/g, function (key) {
                wrapCss[key] = getCss[key];
            });

            for (var key in wrapCss)
                $wrap.style[key] = wrapCss[key];

            // give the source element some saner styles
            setCss.border  = setCss.margin = setCss.padding = 0;
            setCss.display = 'block';
            setCss.opacity = 1;

            // set up the event handlers
            $el.addEventListener('loadedmetadata', doWork);
            window.addEventListener('optimizedResize', doWork);

            // we may have missed the loadedmetadata event, so if the video has loaded
            // enough data, just drop the event listener and execute
            if ($el.readyState >= 1) {
                $el.removeEventListener('loadedmetadata', doWork);
                doWork();
            }

            /**
     * Do the actual sizing. Math.
     * @methodOf fitIt
     */
            function doWork () {
                // the actual size and ratio of the video
                // we do this here, even though it doesn't change, because
                // at this point we can be sure the metadata has loaded
                var videoWidth  = $el.videoWidth,
                    videoHeight = $el.videoHeight,
                    videoRatio  = videoWidth / videoHeight;

                var wrapWidth  = $wrap.clientWidth,
                    wrapHeight = $wrap.clientHeight,
                    wrapRatio  = wrapWidth / wrapHeight;

                var newHeight = 0,
                    newWidth  = 0;
                setCss.marginLeft = setCss.marginTop = 0;

                // basically we do the opposite action for contain and cover,
                // depending on whether the video aspect ratio is less than or
                // greater than the wrapper's aspect ratio
                if (videoRatio < wrapRatio ?
                    style['object-fit'] === 'contain' : style['object-fit'] === 'cover') {
                    newHeight = wrapHeight * videoRatio;
                    newWidth  = wrapWidth / videoRatio;

                    setCss.width  = Math.round(newHeight) + 'px';
                    setCss.height = wrapHeight + 'px';

                    if (style['object-position-x'] === 'left')
                        setCss.marginLeft = 0;
                    else if (style['object-position-x'] === 'right')
                        setCss.marginLeft = Math.round(wrapWidth - newHeight) + 'px';
                    else
                        setCss.marginLeft = Math.round((wrapWidth - newHeight) / 2) + 'px';
                } else {
                    newWidth = wrapWidth / videoRatio;

                    setCss.width     = wrapWidth + 'px';
                    setCss.height    = Math.round(newWidth) + 'px';

                    if (style['object-position-y'] === 'top')
                        setCss.marginTop = 0;
                    else if (style['object-position-y'] === 'bottom')
                        setCss.marginTop = Math.round(wrapHeight - newWidth) + 'px';
                    else
                        setCss.marginTop = Math.round((wrapHeight - newWidth) / 2) + 'px';
                }

                // play the video if autoplay is set
                if ($el.autoplay)
                    $el.play();
            }
        }

        /**
   * Split the object-position property into x and y position properties
   * @param  {object} style Relevant element styles
   * @return {object}       The style object with the added x and y props
   */
        function parsePosition (style) {
            if (~style['object-position'].indexOf('left'))
                style['object-position-x'] = 'left';
            else if (~style['object-position'].indexOf('right'))
                style['object-position-x'] = 'right';
            else
                style['object-position-x'] = 'center';

            if (~style['object-position'].indexOf('top'))
                style['object-position-y'] = 'top';
            else if (~style['object-position'].indexOf('bottom'))
                style['object-position-y'] = 'bottom';
            else
                style['object-position-y'] = 'center';

            return style;
        }

        /**
   * Throttle an event with RequestAnimationFrame API for better performance
   * @param  {string} type The event to throttle
   * @param  {string} name Custom event name to listen for
   * @param  {object} obj  Optional object to attach the event to
   */
        function throttle (type, name, obj) {
            obj = obj || window;
            var running = false,
                evt     = null;

            // IE does not support the CustomEvent constructor
            // so if that fails do it the old way
            try {
                evt = new CustomEvent(name);
            } catch (e) {
                evt = document.createEvent('Event');
                evt.initEvent(name, true, true);
            }

            var func = function () {
                if (running) return;

                running = true;
                requestAnimationFrame(function () {
                    obj.dispatchEvent(evt);
                    running = false;
                });
            };

            obj.addEventListener(type, func);
        }
    };

    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports = objectFitVideos;
    }

    else if (window) {
        window.objectFitVideos = objectFitVideos;
    }
})(window, document);
