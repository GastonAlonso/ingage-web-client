'use strict';

(function (window) {
    document.addEventListener('DOMContentLoaded', function () {
        var pages = window.$$('.page--points-of-interest');

        function initPointsOfInterest (page) {
            var root = page.querySelector('.lazy-image');

            // Safely get and parse attributes
            var attributes = {};
            try {
                if (root.dataset.attributes) {
                    attributes = JSON.parse(root.dataset.attributes);
                }
            } catch (error) {
                // catch error
            }

            window.sm.lazyImage({
                root: root,
                image: {
                    src: root.dataset.lazySrc,
                    attributes: attributes
                },
                sentinels: {
                    before: root.querySelector('.sentinel--before'),
                    after: root.querySelector('.sentinel--after')
                }
            });
        }

        for (var i = 0; i < pages.length; i++) {
            initPointsOfInterest(pages[i]);
        }
    });
})(window);
