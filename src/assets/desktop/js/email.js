'use strict';

(function (window, document) {
    function isValidName (name) {
        return name.length >= 1 &&
               name.length <=32 &&
               // regex for ascii
               /^[\x00-\xFF]*$/.test(name);
    }

    function isValidEmail (email) {
        // regex from http://emailregex.com/
        return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email)
    }

    document.addEventListener('DOMContentLoaded', function () {
        var image = window.$('.page--email img[data-lazy-src]');
        if (image) {
            image.src = image.dataset.lazySrc;
        }

        var emailPages = window.$$('.page--email');

        for (var i = 0; i < emailPages.length; i++) {
            var emailPage = emailPages[i];

            initImageEmail(emailPage);
        }

        function initImageEmail (image) {
            var root = image.querySelector('.lazy-image');

            window.sm.lazyImage({
                root: root,
                image: {
                    src: root.dataset.lazySrc,
                    attributes: {}
                },
                sentinels: {
                    before: root.querySelector('.sentinel--before'),
                    after: root.querySelector('.sentinel--after')
                }
            });
        }

        // get each email form on the page
        var widgets = window.$$('.page--email form');
        for (var i = 0, len = widgets.length; i < len; i++) {
            var widget = widgets[i];

            initEmailWidget(widget);
        }

        function initEmailWidget (widget) {
            // input fields
            var firstNameInput = widget.querySelector('input[name="FNAME"]');
            var lastNameInput = widget.querySelector('input[name="LNAME"]');
            var emailInput = widget.querySelector('input[name="EMAIL"]');

            // state
            var valid = false;

            function validateForm () {
                valid = isValidName(firstNameInput.value) &&
                        isValidName(lastNameInput.value) &&
                        isValidEmail(emailInput.value);
            }

            // TODO: Let's throttle this in the future
            firstNameInput.addEventListener('change', validateForm);
            lastNameInput.addEventListener('change', validateForm);
            emailInput.addEventListener('change', validateForm);

            widget.addEventListener('submit', function (e) {
                // do not submit without valid data
                if (!valid) {
                    e.preventDefault();

                    return;
                }

                window.sm.analytics.send({
                    action: window.sm.analytics.METRIC_EVENT.SENT_CONTACT_INFO
                });
            });
        }
    });
})(window, document);
