(function (window) {
    document.addEventListener('DOMContentLoaded', function () {
        var widgets  = window.$$('.page--scrollmotion .page__media');

        for (var i = 0; i < widgets.length; i++) {
            initScrollmotionWidget(widgets[i]);
        }
    });

    function initScrollmotionWidget (widget) {
        var slider = widget.querySelector('.scrollmotion__slider');
        var indicator = widget.querySelector('.progress-indicator__progress');

        // Convert array-like into array.
        var container = widget.querySelector('.scrollmotion__images');
        var url = container.dataset.url;

        // images loaded so far
        var imagesLoaded = 0;

        // index of currently active frame
        var activeFrameIndex = 0;

        // pixels to scrub to change one frame
        var PIXELS_PER_FRAME = 10;

        // active frame when scrub starts
        var startScrubFrame;

        // load images
        loadImages(url, makeWidgetInteractive);

        function makeWidgetInteractive (error, total) {
            if (error) {
                console.error(error);
                return;
            }

            // preload frame easing function
            var easeToFrame = easeToActiveFrame(activeFrameIndex, function (activeFrameIndex) {
                return setActiveFrame(activeFrameIndex, total);
            });

            // make images scrubbable
            scrubbable({
                el: widget.querySelector('.scrollmotion__images'),

                onStart: function () {
                    // keep track of current active frame
                    startScrubFrame = activeFrameIndex;

                    window.sm.analytics.send({
                        action: window.sm.analytics.METRIC_EVENT.PLAY_SCROLLMOTION
                    });
                },

                onMove: function (delta) {
                    if (delta.x < PIXELS_PER_FRAME || delta.x > PIXELS_PER_FRAME) {
                        // difference in frames from start frame
                        var frameOffset = Math.floor(delta.x / PIXELS_PER_FRAME);

                        // calculate percent of track handle progresses to
                        var handleOffset = (activeFrameIndex / (total - 1)) * 100;

                        // limit progess to available frames
                        activeFrameIndex = window.clamp(0, total - 1, startScrubFrame + frameOffset);

                        // ease to current active frame
                        easeToFrame(activeFrameIndex);

                        // apply percentage to slider style
                        slider.style.transform = 'translateX(' + handleOffset + '%)';
                    }
                }
            });

            // make handle draggable
            draggable({
                handle: widget.querySelector('.scrollmotion__handle'),

                container: widget.querySelector('.scrollmotion__range'),

                onStart: function () {
                    window.sm.analytics.send({
                        action: window.sm.analytics.METRIC_EVENT.PLAY_SCROLLMOTION
                    });
                },

                onMove: function (position, percentage) {
                    // apply percentage to slider style
                    slider.style.transform = 'translateX(' + percentage.x + '%)';

                    // get current active frame
                    activeFrameIndex = getActiveFrameIndex(percentage, total);

                    // ease to current active frame
                    easeToFrame(activeFrameIndex);
                }
            });
        }

        /**
         * load images
         */
        function loadImages (url, callback) {
            // Request array of image url's
            window.sm.http({
                method: 'get',
                url: url,
                onLoad: function (request, event) {
                    var response = request.response;
                    var images = JSON.parse(response);

                    for (var i = 0; i < images.length; i++) {
                        loadImage(images[i], i === 0, images.length);
                    }

                    callback(null, images.length);
                },
                onError: function (request, error) {
                    callback(error);
                }
            });
        }

        /*
         * Load a single image frame
         */
        function loadImage (image, active, total) {
            var imageContainer = document.createElement('div');
            var imageElement = document.createElement('img');

            imageElement.onload = window.sm.bind(imageLoaded, total);
            imageElement.onerror = window.sm.bind(imageLoaded, total);

            imageElement.src = image;
            imageContainer.classList.add('scrollmotion__frame');

            if (active) {
                imageContainer.classList.add('scrollmotion__frame--active');
            }

            imageContainer.appendChild(imageElement);
            container.appendChild(imageContainer);
        }

        /**
         * Keep track of images loaded
         */
        function imageLoaded (total) {
            imagesLoaded++;

            // circumference of progress indicator
            var circumference = Math.PI * 128;

            // loading progress
            var progress = (imagesLoaded / total) * 100;

            // update progress indicator
            indicator.style.strokeDashoffset = (100 - progress) / 100 * circumference;

            // remove progress indicator when all images load
            if (imagesLoaded === total) {
                widget.classList.remove('page__media--loading');
            }
        }

        /**
         * Get the current active frame based on slide percentage
         */
        function getActiveFrameIndex (percentage, total) {
            var activeFrameIndex = Math.floor(total * (percentage.x / 100));

            return window.clamp(0, total - 1, activeFrameIndex);
        }

        /**
         * Show the current active frame, hide the rest
         */
        function setActiveFrame (activeFrameIndex, total) {
            var images = container.querySelectorAll('.scrollmotion__frame');
            for (var i = 0; i < total; i++) {
                if (i === activeFrameIndex) {
                    images[i].classList.add('scrollmotion__frame--active');
                }

                else {
                    images[i].classList.remove('scrollmotion__frame--active');
                }
            }
        }
    }

    function easeToActiveFrame (startIndex, callback) {
        // remember animation frame request id
        var requestId;

        // current frame displayed
        var currentIndex = startIndex;

        return function (targetIndex) {
            // current easing step
            var currentStep = 0;

            // change in index per step
            var changeInIndex = 20;

            // total easing steps
            var totalSteps = 20;

            /**
             * Ease out quadratic function
             */
            var easeOutQuad = function (t, b, c, d) {
                return -c * (t /= d) * (t - 2) + b;
            };

            // cancel current request if exists
            if (requestId) {
                window.cancelAnimationFrame(requestId);
            }

            // target index is greater, ease forward to it
            if (currentIndex <= targetIndex) {
                easeForward();
            }

            // otherwise ease backward
            else {
                easeBackward();
            }

            function easeForward () {
                // get next index from the easing function
                var nextIndex = easeOutQuad(
                    currentStep++,
                    currentIndex,
                    changeInIndex,
                    totalSteps
                );

                // ease until target index
                if (nextIndex < targetIndex) {
                    setNextIndex(nextIndex);

                    requestId = window.requestAnimationFrame(easeForward);
                }

                // stop easing when target is reached
                else {
                    setNextIndex(targetIndex);
                }
            }

            function easeBackward () {
                // get next index from the easing function
                var nextIndex = easeOutQuad(
                    currentStep++,
                    currentIndex,
                    -changeInIndex,
                    totalSteps
                );

                // repeat until target index
                if (nextIndex > targetIndex) {
                    setNextIndex(nextIndex);

                    requestId = window.requestAnimationFrame(easeBackward);
                }

                // stop easing when target is reached
                else {
                    setNextIndex(targetIndex);
                }
            }

            function setNextIndex (nextIndex) {
                currentIndex = nextIndex;

                callback(Math.floor(nextIndex));
            }
        };
    }

    /**
     * Make given element draggable
     */
    function draggable (options) {
        // draggable element
        var handle = options.handle;

        // container element
        var container = options.container;

        // dragging lifecycle callbacks
        var startCallback = options.onStart || function () {};
        var moveCallback = options.onMove || function () {};
        var stopCallback = options.onStop || function () {};

        // touch identifier
        var touchId;

        // container dimensions
        var containerDims = null;

        // handle dimensions
        var handleDims = null;

        // distance from handle center
        var centerAdjust = null;

        var handleMouseDown = function (evt) {
            var position = { x: evt.clientX, y: evt.clientY };

            document.body.classList.add('disable-user-select');

            handleStart(position);

            // add document listeners after interaction starts
            document.addEventListener('mousemove', handleMouseMove);
            document.addEventListener('mouseup', handleMouseUp);
        };

        var handleMouseMove = function (evt) {
            var position = { x: evt.clientX, y: evt.clientY };

            handleMove(position);
        };

        var handleMouseUp = function (evt) {
            var position = { x: evt.clientX, y: evt.clientY };

            document.body.classList.remove('disable-user-select');

            handleStop(position);

            // remove document listeners when interaction stops
            document.removeEventListener('mousemove', handleMouseMove);
            document.removeEventListener('mouseup', handleMouseUp);
        };

        var handleTouchStart = function (evt) {
            // save touch identifier
            touchId = getTouchId(evt);

            // find touch by id
            var touch = findTouch(evt, touchId);

            // return if no touch was found
            if (!touch) return;

            var position = { x: touch.clientX, y: touch.clientY };

            // prevent scrolling when
            // dragging using touch
            evt.preventDefault();

            handleStart(position);

            // add document listeners after interaction starts
            document.addEventListener('touchmove', handleTouchMove);
            document.addEventListener('touchend', handleTouchEnd);
        };

        var handleTouchMove = function (evt) {
            // find touch by id
            var touch = findTouch(evt, touchId);

            // return if no touch was found
            if (!touch) return;

            var position = { x: touch.clientX, y: touch.clientY };

            handleMove(position);
        };

        var handleTouchEnd = function (evt) {
            // find touch by id
            var touch = findTouch(evt, touchId);

            // return if no touch was found
            if (!touch) return;

            var position = { x: touch.clientX, y: touch.clientY };

            handleStop(position);

            // remove document listeners when interaction stops
            document.removeEventListener('touchmove', handleTouchMove);
            document.removeEventListener('touchend', handleTouchEnd);
        };

        var handleStart = function (position) {
            // get handle and container dimensions on start
            containerDims = container.getBoundingClientRect();
            handleDims = handle.getBoundingClientRect();

            // get distance from handle center to adjust event position
            centerAdjust = getCenterAdjust(position, handleDims);

            // get current event position
            var percentage = getPercentage(position, centerAdjust, containerDims);

            startCallback(position, percentage);
        };

        var handleMove = function (position) {
            // get current event position
            var percentage = getPercentage(position, centerAdjust, containerDims);

            moveCallback(position, percentage);
        };

        var handleStop = function (position) {
            // get current event position
            var percentage = getPercentage(position, centerAdjust, containerDims);

            stopCallback(position, percentage);
        };

        // initialize element event listeners
        handle.addEventListener('touchstart', handleTouchStart);
        handle.addEventListener('mousedown', handleMouseDown);
    }

    /**
     * Make given element scrubbable
     */
    function scrubbable (options) {
        // scrubbable element
        var el = options.el;

        // touch event hooks
        var onStart = options.onStart || function () {};
        var onMove = options.onMove || function () {};

        // touch identifier
        var touchId;

        // touch start position
        var startPosition;

        var handleTouchStart = function (evt) {
            // save touch identifier
            touchId = getTouchId(evt);

            // find touch by id
            var touch = findTouch(evt, touchId);

            // return if no touch was found
            if (!touch) return;

            // kepp track of start position
            startPosition = { x: touch.clientX, y: touch.clientY };

            onStart();
        };

        var handleTouchMove = function (evt) {
            // find touch by id
            var touch = findTouch(evt, touchId);

            // return if no touch was found
            if (!touch) return;

            // current event position
            var currentPosition = { x: touch.clientX, y: touch.clientY };

            // the difference in position of start event
            var delta = {
                x: currentPosition.x - startPosition.x,
                y: currentPosition.y - startPosition.y
            };

            onMove(delta);
        };

        el.addEventListener('touchstart', handleTouchStart);
        el.addEventListener('touchmove', handleTouchMove);
    }

    /**
     * Gets id of the original touch event
     */
    function getTouchId (evt) {
        if (evt.targetTouches && evt.targetTouches[0]) {
            return evt.targetTouches[0].identifier;
        }

        if (evt.changedTouches && evt.changedTouches[0]) {
            return evt.targetTouches[0].identifier;
        }
    }

    /**
     * Finds touch event with the given id
     */
    function findTouch (evt, touchId) {
        var touchIdMatches = function (touch) {
            return touch.identifier === touchId;
        };

        return (evt.targetTouches && window.find(evt.targetTouches, touchIdMatches))
            || (evt.changedTouches && window.find(evt.changedTouches, touchIdMatches));
    }

    /**
     * Get distance from the handle center point
     * to the position of the start event.
     */
    function getCenterAdjust (position, handleDims) {
        // get handle dimensions
        var top = handleDims.top;
        var left = handleDims.left;
        var width = handleDims.width;
        var height = handleDims.height;

        // Calculate handle center position
        var centerX = left + width / 2;
        var centerY = top + height / 2;

        // return the difference between
        // event position and center coords
        return {
            x: position.x - centerX,
            y: position.y - centerY
        };
    }

    /**
     * Get event position as percentage relative to container
     */
    function getPercentage (position, centerAdjust, containerDims) {
        // adjust coordinates to handle center
        var adjustedX = position.x - centerAdjust.x;
        var adjustedY = position.y - centerAdjust.y;

        // get container dimensions
        var top = containerDims.top;
        var left = containerDims.left;
        var width = containerDims.width;
        var height = containerDims.height;

        // calculate slide percentage based on
        // event position and container dimensions
        var xPercent = ((adjustedX - left) / width) * 100;
        var yPercent = ((adjustedY - top) / height) * 100;

        // ignore mouse movement outside container
        return {
            x: window.clamp(0, 100, xPercent),
            y: window.clamp(0, 100, yPercent)
        };
    }
})(window);
