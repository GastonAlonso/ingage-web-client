'use strict';

(function (window) {
    document.addEventListener('DOMContentLoaded', function () {
        var overlays = window.$$('.point-overlay');

        for (var i = 0; i < overlays.length; i++) {
            initPointsOverlay(overlays[i]);
        }
    });

    /**
     * Initialize points overlay interactivity
     */
    function initPointsOverlay (overlay) {
        // elements
        var points = overlay.querySelectorAll('.point');
        var gallery = overlay.querySelector('.point-gallery');

        // gallery buttons
        var previous = overlay.querySelector('.gallery-button__previous');
        var next = overlay.querySelector('.gallery-button__next');
        var close = overlay.querySelector('.gallery-button__close');

        // gallery elements
        var position = overlay.querySelector('.position');
        var slides = gallery.querySelectorAll('.slide');

        var currentSlide = 0;

        var handleCloseClick = function () {
            // hide gallery
            gallery.classList.add('hidden');

            // reset slide
            resetSlide(currentSlide);

            // allow scrolling
            document.body.classList.remove('disable-scroll');

            // remove listeners
            close.removeEventListener('click', handleCloseClick);
            previous.removeEventListener('click', handlePreviousClick);
            next.removeEventListener('click', handleNextClick);
        };

        var handleVideoEnded = function () {
            // get play button
            var playButton = this.parentNode.querySelector('.play-button');

            // restart video
            this.currentTime = 0;

            // hide controls
            this.controls = false;

            // show play button
            playButton.classList.remove('hidden');
        };

        var handleVideoPlayClick = function () {
            // get video element
            var video = this.parentNode.querySelector('.video');

            // hide play button
            this.classList.add('hidden');

            // add attributes
            video.controls = true;

            // play video
            video.play();

            // add listener
            video.addEventListener('ended', handleVideoEnded);

            // send analytics event
            window.sm.analytics.send({
                action: window.sm.analytics.METRIC_EVENT.PLAY_POI_VIDEO
            });
        };

        var handleAudioEnded = function () {
            // get audio button
            var audioButton = this.parentNode.querySelector('.audio-button');

            // remove playing class
            audioButton.classList.remove('audio-button--playing');
        };

        var handleAudioClick = function () {
            // get audio element
            var audio = this.parentNode.querySelector('.audio');

            // pause audio if playing
            if (audio.currentTime !== 0 && !audio.paused) {
                // remove playing class
                this.classList.remove('audio-button--playing');

                audio.pause();
            }

            // play audio
            else {
                // add playing class
                this.classList.add('audio-button--playing');

                audio.play();

                // send analytics event
                window.sm.analytics.send({
                    action: window.sm.analytics.METRIC_EVENT.PLAY_POI_AUDIO
                });
            }
        };

        var generateMapUrl = function (data, embed) {
            var name = data.name || '';
            var address = data.address || '';

            return embed ? `//www.google.com/maps/embed/v1/place?q=${encodeURIComponent(name + ' ' + address)}&key=AIzaSyCWSXr0q_A-fQSGuFUEGcT-LEPjZZB_0pU` : `https://www.google.com/maps/search/?api=1&query=${encodeURIComponent(name + ' ' + address)}`;
        };

        var renderSlide = function (index, playAudio) {
            var slide = slides[index];

            switch (slide.dataset.type) {
                case 'image':
                    var image = slide.querySelector('.image');

                    if (!image.src) {
                        // set image source
                        image.src = image.dataset.image;
                    }

                    break;

                case 'video':
                    var video = slide.querySelector('.video');
                    var playButton = slide.querySelector('.play-button');

                    if (!video.src) {
                        // set video source
                        video.src = video.dataset.video;
                    }

                    // add listener
                    playButton.addEventListener('click', handleVideoPlayClick);
                    break;

                case 'audio':
                    var audio = slide.querySelector('.audio');
                    var audioButton = slide.querySelector('.audio-button');

                    // set audio source
                    if (!audio.src) {
                        audio.src = audio.dataset.audio;
                    }

                    // play audio if audio point was clicked
                    if (playAudio) {
                        audio.play();

                        // add playing class
                        audioButton.classList.add('audio-button--playing');
                    }

                    // add listeners
                    audioButton.addEventListener('click', handleAudioClick);
                    audio.addEventListener('ended', handleAudioEnded);

                    break;

                case 'map':
                    var map = slide.querySelector('.map');
                    var link = slide.querySelector('.body');
                    var data = JSON.parse(map.dataset.data);

                    if (!map.src) {
                        map.src = generateMapUrl(data, true);

                        link.href = generateMapUrl(data);
                    }

                    break;

                default:
                    break;
            }

            // show slide
            slide.classList.remove('hidden');
        };

        var updatePositionText = function (index) {
            position.innerHTML = `${index + 1} of ${slides.length}`;
        };

        var resetSlide = function (index) {
            var slide = slides[index];

            // reset video
            if (slide.dataset.type === 'video') {
                var video = slide.querySelector('.video');
                var playButton = slide.querySelector('.play-button');

                // pause video
                video.pause();

                // restart video
                video.currentTime = 0;

                // hide controls
                video.controls = false;

                // show play button
                playButton.classList.remove('hidden');

                // remove listeners
                playButton.removeEventListener('click', handleVideoPlayClick);
                video.removeEventListener('ended', handleVideoEnded);
            }

            // reset audio
            if (slide.dataset.type === 'audio') {
                var audio = slide.querySelector('.audio');
                var audioButton = slide.querySelector('.audio-button');

                // pause audio
                audio.pause();

                // restart audio
                audio.currentTime = 0;

                // remove class
                audioButton.classList.remove('audio-button--playing');

                // remove listeners
                audioButton.removeEventListener('click', handleAudioClick);
                audio.removeEventListener('ended', handleAudioEnded);
            }

            // hide slide
            slides[index].classList.add('hidden');
        };

        var handlePreviousClick = function () {
            // reset current slide
            resetSlide(currentSlide);

            // at beginning, go to end
            if (currentSlide === 0) {
                currentSlide = slides.length - 1;
            }

            // decrement
            else {
                currentSlide--;
            }

            // show previous slide
            renderSlide(currentSlide);

            // update position
            updatePositionText(currentSlide);
        };

        var handleNextClick = function () {
            // reset current slide
            resetSlide(currentSlide);

            // at end, restart
            if (currentSlide === slides.length - 1) {
                currentSlide = 0;
            }

            // increment
            else {
                currentSlide++;
            }

            // show next slide
            renderSlide(currentSlide);

            // update position
            updatePositionText(currentSlide);
        };

        var handlePointClick = function () {
            var type = this.dataset.type;

            // send analytics event
            sendOpenEvent(type);

            // website point does not open in gallery
            if (type === 'website') {
                return;
            }

            // get index
            currentSlide = parseInt(this.dataset.slideIndex);

            // show correct slide, play audio if audio point
            renderSlide(currentSlide, true);

            // update text
            updatePositionText(currentSlide);

            // show gallery
            gallery.classList.remove('hidden');

            // disable scrolling
            document.body.classList.add('disable-scroll');

            // add listeners
            close.addEventListener('click', handleCloseClick);
            previous.addEventListener('click', handlePreviousClick);
            next.addEventListener('click', handleNextClick);
        };

        var sendOpenEvent = function (type) {
            var action = null;

            // determina action for point type
            switch (type) {
                case 'blurb':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_TEXT;
                    break;
                case 'image':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_IMAGE;
                    break;
                case 'map':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_MAP;
                    break;
                case 'email':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_EMAIL;
                    break;
                case 'video':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_VIDEO;
                    break;
                case 'audio':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_AUDIO;
                    break;
                case 'website':
                    action = window.sm.analytics.METRIC_EVENT.OPEN_POI_WEBSITE;
                    break;
            }

            // send event
            if (action) {
                window.sm.analytics.send({ action: action });
            }
        };

        // add event listeners to points
        for (var i = 0; i < points.length; i++) {
            points[i].addEventListener('click', handlePointClick);
        }
    }
})(window);
