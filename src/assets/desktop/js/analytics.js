'use strict';

(function (window, document) {
    document.addEventListener('DOMContentLoaded', function () {
        // initialize keen
        KeenAnalytics.init();

        // send view story event on load
        window.sm.analytics.send({
            action: window.sm.analytics.METRIC_EVENT.VIEW_STORY
        });

        // send view story end event on unload
        window.addEventListener('beforeunload', function () {
            window.sm.analytics.send({
                action: window.sm.analytics.METRIC_EVENT.VIEW_STORY_END
            });
        });
    });

    /**
     * @global {Object} sm
     * @description Global ScrollMotion namespace
     */
    window.sm = window.sm || {};

    /**
     * @namespace {Object} sm.analytics
     * @description Analytics namespace
     */
    window.sm.analytics = window.sm.analytics || {};

    /**
     * @constant {String} METRIC_CATEGORY
     * @description default event category
     */
    window.sm.analytics.METRIC_CATEGORY = 'web-viewer';

    /**
     * @constant {Object} METRIC_EVENT
     * @description analytic event actions
     */
    window.sm.analytics.METRIC_EVENT = {
        // story
        VIEW_STORY: 'view-story',
        VIEW_STORY_END: 'view-story-end',

        // share
        SHARE_FACEBOOK: 'share-facebook',
        SHARE_TWITTER: 'share-twitter',
        SHARE_EMAIL: 'share-email',

        // cta
        CLICK_LEARN_MORE: 'click-learn-more',
        CLICK_RETURN_TOP: 'click-return-to-the-top',
        CLICK_MADE_WITH: 'click-made-with-ingage',

        // navigation
        USE_NAV: 'use-nav',

        // email contact
        SENT_CONTACT_INFO: 'sent-contact-info',

        // compare
        MOVE_COMPARE: 'move-compare',

        // poi
        OPEN_POI_TEXT: 'open-poi-text',
        OPEN_POI_IMAGE: 'open-poi-image',
        OPEN_POI_VIDEO: 'open-poi-video',
        PLAY_POI_VIDEO: 'play-poi-video',
        OPEN_POI_AUDIO: 'open-poi-audio',
        PLAY_POI_AUDIO: 'play-poi-audio',
        OPEN_POI_EMAIL: 'open-poi-email',
        OPEN_POI_MAP: 'open-poi-map',
        OPEN_POI_WEBSITE: 'open-poi-website',

        // scrollmotion
        PLAY_SCROLLMOTION: 'play-scrollmotion',

        // video
        PLAY_VIDEO: 'play-video'
    };

    /**
     * @method sm.analytics.send
     * @description Routes analytic events to the available analytics services
     *
     * @param {Object} evt event properties
     * @param {String} evt.category the event category
     * @param {String} evt.action the event action
     */
    window.sm.analytics.send = function (evt) {
        GoogleAnalytics.send(evt);
        MixpanelAnalytics.send(evt);
        KeenAnalytics.send(evt);
    };

    /**
     * @class GoogleAnalytics
     * @description Sends google analytics events
     */
    var GoogleAnalytics = {

        /**
         * @method send
         * @description send event
         */
        send: function (evt) {
            var category = evt.category || window.sm.analytics.METRIC_CATEGORY;
            var action = evt.action || 'Testing';

            if (window.ga) {
                window.ga('send', 'event', category, action);
            }
        }
    };

    /**
     * @class MixpanelAnalytics
     * @description Sends mixpanel analytics events
     */
    var MixpanelAnalytics = {

        /**
         * @method send
         * @description send event
         */
        send: function (evt) {
            var category = evt.category || window.sm.analytics.METRIC_CATEGORY;
            var action = evt.action || 'Testing';

            if (window.mixpanel) {
                window.mixpanel.track(action, {
                    eventCategory: category,
                    storyName: window.sm.storyName
                });
            }
        }
    };

    /**
     * @class KeenAnalytics
     * @description Sends keen analytics events
     */
    var KeenAnalytics = {

        /**
         * @method init
         * @description initialize keen
         */
        init: function () {
            var KEEN_KEYS = {
                dev: {
                    projectId: '595e7ee5c9e77c0001419159',
                    readKey:   '3EBB7D54583B317915DB7169399AF7248C42E7603E02B61727A7E4BDF526F1CE8C6142F77BF1A272192CE8DE35FD629B9DA346065A0A28235BED8BC937CC842E8D0ED1DDA8CAD667C17A543EA2DB21D15CFC2E5C7F425FDEE43AF7CE0FC0CE5A',
                    writeKey:  '58ACD600A4DDCD2CDD0915380EEE169B1B8ABF4005153E35B91E3371A3624A4E0E627798EB885F68A15A39CE69200D551AC9579A41F5FEE37DF68B89E750FB9119A022F735398FF19872F533F303CC09AF6631D0D87FADDAAF4F022CC6BF7BFC'
                },
                prod: {
                    projectId: '595e7efec9e77c000140967c',
                    readKey:   'B0AEE37FE673DF6AD69FF97AEED7B483D7F3F7522F27B591DE1C5D1BF7226F0E8F44DB6DAFA5AC504045C0C5D88469272A57D3F4F4DB0B5652E48430AF62E4FB0BFB6C1DC190AF5ED26C505C8FA84D01D035D58D76B2A50249D1A95D4CC07FE5',
                    writeKey:  '9300E3C33DACAA2AE358FF6F2870D18B55A4511C2C37556AB0EAB0790C932B47EA453307608CA74CB61D4A2950CE37FA3B001A7374150AD0A7B43FA1970BF73387B961E7860C7869EB1DB43DF0524FBA4AA250B822AED70B9BAB3D8A48276862'
                }
            };

            this.startTime = Date.now();

            var envKeys = window.sm.environment === 'production'
                ? KEEN_KEYS.prod
                : KEEN_KEYS.dev;

            this.keenClient = new window.Keen({
                projectId: envKeys.projectId,
                readKey: envKeys.readKey,
                writeKey: envKeys.writeKey
            });
        },

        /**
         * @method buildEvent
         * @description add enviromnent properties to event
         */
        buildEvent: function (properties) {
            var author = window.sm.author;
            var storyName = window.sm.storyName;
            var contentId = window.sm.contentId;
            var platform = window.sm.platform;

            properties.story_author_id = author;
            properties.story_name = storyName;
            properties.story_id = contentId;
            properties.platform = 'web';
            properties.event_owner_id = author;
            properties.os = platform.os;
            properties.os_version = platform.osVersion;
            properties.device = `${platform.browser} ${platform.browserMajorVersion} on ${platform.os} ${platform.osVersion}`;

            return properties;
        },

        /**
         * @method send
         * @description send event
         */
        send: function (evt) {
            var collection = window.sm.author;
            var eventProperties = {};

            switch (evt.action) {
                case window.sm.analytics.METRIC_EVENT.VIEW_STORY:
                    collection += '+story_view_begin';
                    break;

                case window.sm.analytics.METRIC_EVENT.VIEW_STORY_END:
                    collection += '+story_view_end';
                    eventProperties.view_length = (Date.now() - this.startTime) / 1000;
                    break;

                case window.sm.analytics.METRIC_EVENT.SHARE_FACEBOOK:
                    collection += '+story_share';
                    eventProperties.share_type = 'Facebook';
                    break;

                case window.sm.analytics.METRIC_EVENT.SHARE_TWITTER:
                    collection += '+story_share';
                    eventProperties.share_type = 'Twitter';
                    break;

                case window.sm.analytics.METRIC_EVENT.SHARE_EMAIL:
                    collection += '+story_share';
                    eventProperties.share_type = 'Email';
                    break;

                case window.sm.analytics.METRIC_EVENT.SHARE_FACEBOOK_MESSENGER:
                    collection += '+story_share';
                    eventProperties.share_type = 'Facebook Messenger';
                    break;
            }

            this.keenClient.addEvent(collection, this.buildEvent(eventProperties));
        }
    };
})(window, document);
