'use strict';

(function (window) {
    document.addEventListener('DOMContentLoaded', function () {
        var pages = window.$$('.page--text-and-image')

        for (var i = 0; i < pages.length; i++) {
            init(pages[i]);
        }

        function init (page) {
            var root = page.querySelector('.lazy-image');

            window.sm.lazyImage({
                root: root,
                image: {
                    src: root.dataset.lazySrc,
                    attributes: {}
                },
                sentinels: {
                    before: root.querySelector('.sentinel--before'),
                    after: root.querySelector('.sentinel--after')
                }
            })
        }
    });
})(window);
