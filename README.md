Ingage Web Client
=================

The Ingage web client.

### Development

1\. Clone this repository

```shell
git clone https://bitbucket.org/scrollmotiongit/ingage-web-client.git

cd ingage-web-client
```

2\. Build the image

```shell
docker build -f dockerfile-dev -t ingage/web-client:dev .
```

3\. Run the image

```shell
# Either using docker-compose

docker-compose up

# Or manually

# run as built
docker run -it ingage/web-client:dev

# run with live updating of current directory
docker run -it -v `pwd`:/opt/app -v /opt/app/node_modules ingage/web-client:dev
```

### File Structure

```
ingage-web-client/
├── dev/                   (development environment files)
│   ├── stubs/             (page stubs to test with)
│   ├── assets/            (client assets to test with)
│   ├── index.js           (entrypoint for development work)
│   ├── stub.js            (base example stub)
│
├── dist/                   (built client files)
│   ├── assets/             (client assets)
│   │   ├── desktop/        (desktop assets)
│   │   │   ├── css/        (desktop css)
│   │   │   ├── js/         (desktop js)
│   │   │
│   │   ├── mobile/         (mobile assets)
│   │   │   ├── css/        (mobile css)
│   │   │   ├── js/         (mobile js)
│   │   │
│   │   ├── favicon.ico     (site icon)
│
├── showcase/               (preprocessed stories to show off)
│
├── src/                    (client source files)
│   ├── assets/             (client assets)
│   │   ├── desktop/        (desktop assets)
│   │   │   ├── css/        (desktop css)
│   │   │   ├── js/         (desktop js)
│   │   │
│   │   ├── mobile/         (mobile assets)
│   │   │   ├── css/        (mobile css)
│   │   │   ├── js/         (mobile js)
│   │
│   ├── templates/          (pug templates)
│   │   ├── mixins/         (pug mixins)
│   │   │
│   │   ├── pages/          (page templates)
│   │   │   ├── desktop/    (desktop page templates)
│   │   │   ├── mobile/     (mobile page templates)
│   │   │
│   │   ├── ui-components/  (ui component templates)
│   │   ├── index.pug       (main template)
│   │   ├── instant.pug     (instants template)
│   │   ├── story.pug       (story template)
│
├── .dockerignore           (ensure docker ignores certain files)
├── .eslintrc.js            (linting configuration)
├── .gitignore              (ensure git ignores certain files)
├── Dockerfile.dev          (development dockerfile)
├── index.js                (entrypoint)
├── package-lock.json       (npm dependency lock)
├── package.json            (npm package manifest)
├── README.md               (this file)
├── server.mjs              (server entrypoint)
```
