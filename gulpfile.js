const gulp = require('gulp');
const watch = require('gulp-watch');
const batch = require('gulp-batch');
const gulpif = require('gulp-if');
const stylus = require('gulp-stylus');
const minify = require('gulp-minify');
const foreach = require('gulp-foreach');
const plumber = require('gulp-plumber');
const cleanCSS = require('gulp-clean-css');
const sourcemaps = require('gulp-sourcemaps');
const autoprefixer = require('gulp-autoprefixer');

// eslint-disable-next-line no-undef
const isProduction = process.env.NODE_ENV === 'production';

gulp.task('watch', function () {
    watch('src/assets/desktop/css/**/*.styl', batch(function (events, done) {
        gulp.start('css:desktop', done);
    }));

    watch('src/assets/mobile/css/**/*.styl', batch(function (events, done) {
        gulp.start('css:mobile', done);
    }));

    watch('src/assets/desktop/js/**/*.js', batch(function (events, done) {
        gulp.start('js:desktop', done);
    }));

    watch('src/assets/mobile/js/**/*.js', batch(function (events, done) {
        gulp.start('js:mobile', done);
    }));
});

gulp.task('css:mobile', function () {
    gulp.src('src/assets/mobile/css/*.styl')
        .pipe(plumber())
        .pipe(foreach(function (stream) {
            return stream.pipe(gulpif(!isProduction, sourcemaps.init()))
                .pipe(stylus())
                .pipe(autoprefixer({
                    browsers: ['last 2 versions'],
                    cascade: false
                }))
                .pipe(cleanCSS())
                .pipe(gulpif(!isProduction, sourcemaps.write()));
        }))
        .pipe(gulp.dest('dist/assets/mobile/css/'));
});

gulp.task('css:desktop', function () {
    gulp.src('src/assets/desktop/css/*.styl')
        .pipe(plumber())
        .pipe(foreach(function (stream) {
            return stream.pipe(gulpif(!isProduction, sourcemaps.init()))
                .pipe(stylus())
                .pipe(autoprefixer({
                    browsers: ['last 2 versions'],
                    cascade: false
                }))
                .pipe(cleanCSS())
                .pipe(gulpif(!isProduction, sourcemaps.write()));
        }))
        .pipe(gulp.dest('dist/assets/desktop/css/'));
});

gulp.task('js:mobile', function () {
    gulp.src('src/assets/mobile/js/*.js')
        .pipe(plumber())
        .pipe(gulpif(!isProduction, sourcemaps.init()))
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            }
        }))
        .pipe(gulpif(!isProduction, sourcemaps.write()))
        .pipe(gulp.dest('dist/assets/mobile/js/'));
});

gulp.task('js:desktop', function () {
    gulp.src('src/assets/desktop/js/*.js')
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(minify({
            ext: {
                src: '-debug.js',
                min: '.js'
            }
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/assets/desktop/js/'));
});

gulp.task('fonts:mobile', function () {
    gulp.src('src/assets/desktop/fonts/*/**')
        .pipe(plumber())
        .pipe(gulp.dest('dist/assets/mobile/fonts/'));
});

gulp.task('fonts:desktop', function () {
    gulp.src('src/assets/desktop/fonts/*/**')
        .pipe(plumber())
        .pipe(gulp.dest('dist/assets/desktop/fonts/'));
});

gulp.task('mobile', ['css:mobile', 'js:mobile', 'fonts:mobile']);
gulp.task('desktop', ['css:desktop', 'js:desktop', 'fonts:desktop']);

gulp.task('build', ['mobile', 'desktop']);

gulp.task('default', ['build', 'watch']);

