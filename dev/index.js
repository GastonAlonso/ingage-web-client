// Node imports
const path = require('path')

// NPM imports
const pug = require('pug')
const gulp = require('gulp')
const watch = require('gulp-watch')
const batch = require('gulp-batch')
const express = require('express')
const browserSync = require('browser-sync')

// Import test stub
const stub = require('./stub')

// mock story meta
const meta = {
    author: 'https://ssc-api-dev.scrollmotion.com/users/3d46f5e2-fa07-11e6-81c7-0a816a5d7be3/',
    contentId: '8bceddde-3061-11e7-aaf7-0a07b6bd94c4'
}

// Path to static assets
const src = path.resolve(__dirname, '..', 'src')
const dist = path.resolve(__dirname, '..', 'dist')
const devAssets = path.resolve(__dirname, 'assets')

// Path to views directory (templates)
const views = path.resolve(__dirname, '..', 'src', 'templates')

// Import gulpfile
require('../gulpfile')

// Start gulp
gulp.start('default')

// Create server instance
const app = express()

// Create browser-sync instance
const bs = browserSync.create().init({
    // Don't attempt to open a browser
    open: false,
    // Proxy the express server
    proxy: 'localhost:8080',
    // Host on port 3000
    port: 3000,
    // UI settings
    ui: {
        // Host UI on port 3001
        port: 3001
    },
    // Inject script into head
    injectHead: true,
    // Don't print out the <script> snippet in console
    logSnippet: false,
    // Watch all files in src
    files: path.resolve(src, '**/*')
})

// Watch files and reload
watch(path.resolve(src, '**/*.{pug,styl,js}'), batch((events, done) => {
    bs.reload(done)
}))

// Set the view engine
app.set('view engine', 'pug')
app.set('views', views)

// Apply browser-sync middleware to express server
app.use(require('connect-browser-sync')(bs))

// Static assets
app.use(express.static(dist))
app.use('/dev-assets', express.static(devAssets))

// Listen on routes and render the appropriate response
app.get('/', (req, res) => {
    const renderStory = pug.compileFile(
        path.resolve(src, 'templates', 'story.pug'),
        { self: true }
    )

    res.send(
        renderStory({
            config: {
                mobile: false
            },
            stub,
            meta
        })
    )
})

app.get('/mobile', (req, res) => {
    const renderStory = pug.compileFile(
        path.resolve(src, 'templates', 'story.pug'),
        { self: true }
    )

    res.send(
        renderStory({
            config: {
                mobile: true
            },
            stub,
            meta
        })
    )
})

app.get('/scrollmotion', (req, res) => {
    res.json({
        body: {
            images: Array(84).fill(null)
                .map((_, i) =>
                    `/dev-assets/scrollmotion/scrollmotion-${i + 1}.jpg`
                )
        }
    })
})

// Listen on port 8080
app.listen(8080, () => {
    console.log('Server listening on port 8080...')
})
