const textAndImage = require('./stubs/text-and-image')
const compare = require('./stubs/compare')
const scrollmotion = require('./stubs/scrollmotion')
const pointsOfInterest = require('./stubs/points-of-interest')
const video = require('./stubs/video')
const impact = require('./stubs/impact')
const email = require('./stubs/email')

const createLayouts = page =>
    [
        'full',
        'horizontal',
        'vertical-halves',
        'vertical-thirds',
        'square'
    ].map(layout => ({
        ...page,
        properties: {
            ...page.properties,
            layout
        }
    }))

const createImpactLayouts = page =>
    Array(11).fill(null)
        .map((n, i) => ({
            ...page,
            properties: {
                ...page.properties,
                layout: i + 1,
                theme: i % 2 + 1
            }
        }))

module.exports = {
    properties: {
        title: 'Hello World',
        cover: {
            title: 'Personal Services',
            subtitle: 'Makeup By Tiffany',
            layout: 5,
            theme: 2,
            background: '/dev-assets/video-1.mp4',
            _meta: {
                type: 'video/mp4',
                width: 4,
                height: 3
            }
        },
        sections: [
            {
                name: 'Text And Image',
                pages: createLayouts(textAndImage)
            },
            {
                name: 'Compare',
                pages: createLayouts(compare)
            },
            {
                name: 'Scrollmotion',
                pages: createLayouts(scrollmotion)
            },
            {
                name: 'Points of Interest',
                pages: createLayouts(pointsOfInterest)
            },
            {
                name: 'Video',
                pages: [video, video]
            },
            {
                name: 'Impact',
                pages: createImpactLayouts(impact)
            },
            {
                name: 'Email',
                pages: [email]
            }
        ]
    }
}
