const images =
    Array(84).fill(null)
        .map((_, i) => `/dev-assets/scrollmotion/scrollmotion-${i + 1}.jpg`);

module.exports = {
    template: 'flipbook',
    properties: {
        theme: 1,
        layout: 'horizontal',
        images: '/scrollmotion',
        title: 'Gaggles of cool\n people played jack-in-the-box last week with you and me',
        body: 'The hook of the trap\n   song Panda was written about Desiigner\'s affinity for the BMW X6.\n Explained by Desiigner, lyrically the panda is the black X6 and the white X6. The black X6 look like a Phantom, the white X6 look like a panda. Desiigner discovered the song\'s beat on YouTube, which was produced by up-and-coming producer, Menace. '
    }
};

